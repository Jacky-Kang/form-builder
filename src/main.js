// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import Vuelidate from 'vuelidate';
import util from './util';

import Draggable from './components/draggable/Draggable';
import Widget from './components/widget';

import './assets/libs/jquery-1.12.4.min.js';
import './assets/jslib/Verify.js';

Vue.config.productionTip = false;


Vue.use(util);
Vue.use(Vuelidate);
Vue.use(Widget);

Vue.component('Draggable', Draggable);

Vue.filter('zWidget', function (value) {
  return 'Z' + value;
});

Vue.filter('copyJson', function (value) {
  return JSON.parse(JSON.stringify(value));
});

Vue.filter('formatDate', function (value) {
  let date = new Date(value);
  return util.formatDate(date, 'yyyy-MM-dd')
});

Vue.filter('formatTime', function (value) {
  let date = new Date(value);
  return util.formatDate(date, 'hh:mm:ss')
});

Vue.filter('px', function (value) {
  return value + 'px';
});

Vue.filter('em', function (value) {
  return value + 'em';
});

Vue.filter('olo', function (value) {
  return value + '%';
});

Vue.filter('vh', function (value) {
  return value + 'vh';
});

Vue.filter('vw', function (value) {
  return value + 'vw';
});

/**
 * 路由钩子,更改title
 */
router.afterEach((to, from) => {
  document.title = to.meta.title || 'FormBuilder';
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
});
