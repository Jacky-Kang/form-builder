import md5 from 'js-md5'

const Util = {
  md5(value) {
    return md5(value);
  },
  generateId() {
    //12位长唯一字符串
    return (+new Date()).toString(36) + Math.round(Math.random() * 1632959 + 46656).toString(36);
  },
  getFromId(opt, num) {
    return md5(JSON.stringify(opt) + '' + num);
  },

  padLeftZero(str) {
    return ('00' + str).substr(str.length)
  },

  formatDate(date, fmt) {
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    let o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds()
    };
    for (let k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        let str = o[k] + '';
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : this.padLeftZero(str))
      }
    }
    return fmt
  },

  getObjectId() {
    return new Util.ObjectId().toString();
  },

  //2016-5-16 更改为19位长的数字 9位当前时间秒数 3位浏览器特征转换为的机器码 3位随机数为伪进程码 4位为自增长码
  ObjectId: (function () {
    let hashCode = function (str) {
      let hash = 0;
      let i = 0,
        len = str.length;
      for (; i < len; i++) {
        hash = ((hash << 5) ^ (hash >> 27) ^ str.charCodeAt(i)) & 0xFFFFFFF; // 总是得到少于268435455的正数，
      }
      return hash;
    };
    let increment = 0;
    let pid = Math.floor(Math.random() * (1000));
    let machine = hashCode([navigator.userAgent, navigator.language, navigator.language, screen.height, screen.width].join('#')) % 1000;
    if (typeof (localStorage) !== 'undefined') {
      let mongoMachineId = parseInt(localStorage['mongoMachineId']);
      if (mongoMachineId > 0 && mongoMachineId < 1000) {
        machine = Math.floor(localStorage['mongoMachineId']);
      }
      localStorage['mongoMachineId'] = machine;
      document.cookie = 'mongoMachineId=' + machine + ';expires=Tue, 19 Jan 2038 05:00:00 GMT'
    } else {
      let cookieList = document.cookie.split('; ');
      for (let i in cookieList) {
        let cookie = cookieList[i].split('=');
        if (cookie[0] === 'mongoMachineId' && cookie[1] > 0 && cookie[1] < 1000) {
          machine = cookie[1];
          break;
        }
      }
      document.cookie = 'mongoMachineId=' + machine + ';expires=Tue, 19 Jan 2038 05:00:00 GMT';
    }
    return function () {
      if (!(this instanceof Util.ObjectId)) {
        return new Util.ObjectId(arguments[0], arguments[1], arguments[2], arguments[3]).toString();
      }
      if (typeof (arguments[0]) === 'object') {
        this.timestamp = arguments[0].timestamp;
        this.machine = arguments[0].machine;
        this.pid = arguments[0].pid;
        this.increment = arguments[0].increment;
      } else if (typeof (arguments[0]) === 'string' && arguments[0].length === 24) {
        this.timestamp = Number(arguments[0].substr(0, 9));
        this.machine = Number(arguments[0].substr(9, 3));
        this.pid = Number(arguments[0].substr(12, 3));
        this.increment = Number(arguments[0].substr(15, 4));
      } else if (arguments.length === 4 && arguments[0] != null) {
        this.timestamp = arguments[0];
        this.machine = arguments[1];
        this.pid = arguments[2];
        this.increment = arguments[3];
      } else {
        this.timestamp = Math.floor(new Date().valueOf() / 1000);
        if (this.timestamp > 900000000) {
          //只取9位数
          this.timestamp = this.timestamp % 900000000;
        }
        this.machine = machine;
        this.pid = pid;
        if (increment > 9999) {
          increment = 0;
        }
        this.increment = increment++;
      }
    };
  })()
};

Util.ObjectId.prototype.getDate = function () {
  if (+new Date() > 900000000 && this.timestamp <= 900000000) {
    this.timestamp += 900000000;
  }
  return new Date(this.timestamp * 1000);
};

Util.ObjectId.prototype.toString = function () {
  let timestamp = this.timestamp.toString();
  let machine = this.machine.toString();
  let pid = this.pid.toString();
  let increment = this.increment.toString();
  return '000000000'.substr(0, 9 - timestamp.length) + timestamp + '000'.substr(0, 3 - machine.length) + machine + '000'.substr(0, 3 - pid.length) + pid + '0000'.substr(0, 4 - increment.length) + increment;
};

export default Util;
