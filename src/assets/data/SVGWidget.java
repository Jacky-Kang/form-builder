package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class SVGWidget extends Base implements IContainerWidget {
	String getWidgetClass(JSONObject widget) {
		return "pw-svg";
	}

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		if (widget.getString("content") != null) {
			if (widget.getString("aspect_ratio_percent") != null) {
				sb.append("<div class=\"pw-svg-inner\">");
				sb.append(widget.getString("content"));
				sb.append("</div>");
			} else {
				sb.append(widget.getString("content"));
			}
		}
		return sb.toString();
	}
}
