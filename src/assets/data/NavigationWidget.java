package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class NavigationWidget extends Base implements IContainerWidget {
	String getWidgetClass(JSONObject widget) {
		// 在非设计模式下加上样式名 pw-navigation-active，js查找到.pw-navigation-active，对navigation进行初始化。
		return "pw-navigation" + (!isClientSide && !contentForBackend ? " pw-navigation-active" : "");
	}
}
