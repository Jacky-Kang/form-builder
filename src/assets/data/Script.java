package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class Script extends Base {
	public String buildHtml(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		if (widget.getString("script") != null) {
			sb.append("<script>");
			sb.append("(function(){");
			sb.append("'use strict';");
			sb.append(widget.getString("script"));
			sb.append("}).call(document.getElementsByClassName('pw_");
			sb.append(widget.getString("slug"));
			sb.append("')[0]);");
			sb.append("</script>");
		}
		return sb.toString();
	}

}
