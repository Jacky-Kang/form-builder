package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class HTMLWidget extends Base implements IContainerWidget {
	String getWidgetClass(JSONObject widget) {
		return "pw-html pw-wf";
	}

	String getContent(JSONObject widget) {
		if (widget.getString("html") != null) {
			return widget.getString("html");
		}
		return "";
	}
}
