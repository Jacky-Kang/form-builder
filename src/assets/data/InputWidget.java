package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class InputWidget extends Base implements IContainerWidget {
	String getWidgetClass(JSONObject widget) {
		return "pw-input pw-wf";
	}

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		if (widget.getString("input_type").equals("textarea")) {
			sb.append("<textarea");
		} else {
			sb.append("<input type=\"" + widget.getString("input_type") + "\"");
		}
		sb.append(" class=\"");
		if (widget.getString("html_class_string") != null) {
			sb.append(widget.getString("html_class_string") + " ");
		}
		sb.append(getCssClasses(widget));
		sb.append("\"");
		if (widget.getString("input_type").equals("submit")) {
			sb.append(" value=\"" + widget.getString("input_value") + "\"");
		} else {
			if (widget.getString("placeholder") != null) {
				sb.append(" placeholder=\"" + widget.getString("placeholder") + "\"");
			}
			if (widget.getString("input_name") != null) {
				sb.append(" name=\"" + widget.getString("input_name") + "\"");
			}
		}
		if (widget.getString("input_type").equals("textarea")) {
			sb.append("></textarea>");
		} else {
			sb.append(" />");
		}
		return sb.toString();
	}
}
