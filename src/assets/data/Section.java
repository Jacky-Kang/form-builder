package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class Section extends Base implements IContainerWidget {
	public String buildHtml(JSONObject widget) {
		StringBuilder sb = new StringBuilder();

		sb.append(getContent(widget));

		return sb.toString();
	}

}
