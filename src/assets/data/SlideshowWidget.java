package com.zving.pageweaver.widgets;

import java.util.ArrayList;
import java.util.HashMap;

import com.zving.contentcore.template.types.SiteIndexTemplate;
import com.zving.contentcore.util.SiteUtil;
import com.zving.framework.data.DataRow;
import com.zving.framework.data.DataTable;
import com.zving.framework.data.Q;
import com.zving.framework.json.JSONArray;
import com.zving.framework.json.JSONObject;
import com.zving.framework.template.AbstractExecuteContext;
import com.zving.framework.utility.StringUtil;
import com.zving.imageplayer.bl.ImagePlayerBL;
import com.zving.imageplayer.code.ImageSourceCode;
import com.zving.pageweaver.css.Style;
import com.zving.schema.ZCImagePlayer;

public class SlideshowWidget extends Base implements IContainerWidget {
	String getWidgetClass(JSONObject widget) {
		int type = widget.getInt("slideType");
		String result = "pw-slideshow" + (viewedFromOutside ? " swiper-container" : "");
		if (type == 0) {
			result += " pw-base-slideshow";
		}
		if (type == 1) {
			result += " pw-title-slideshow";
		}
		if (type == 2) {
			result += " pw-thumbnail-slideshow";
		}
		return result;
	}

	String getAttributes(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		if (viewedFromOutside) {
			if (widget.getString("autoplay") != null) {
				sb.append(" data-slideshow-autoplay=\"" + widget.getString("autoplay") + "\"");
			}
			if (widget.getString("show_dots") != null) {
				sb.append(" data-slideshow-dots=\"true\"");
			}
		}
		return sb.toString();

	}

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();

		if (viewedFromOutside) {
			sb.append("<div class=\"swiper-wrapper\"></div>");
		}

		boolean first = true;
		JSONArray child = widget.getJSONArray("child_containers");
		JSONObject dynamicConfig = widget.getJSONObject("dynamic_config");
		int slideType = widget.getInt("slideType");
		int dotCount = child.length() - 2;

		if (dynamicConfig != null && dynamicConfig.getInt("imagePlayerID") > 0) {
			DataTable data = getDynamicData(dynamicConfig.getInt("imagePlayerID"));
			setDataTable(data);
			dotCount = getDataTable().getRowCount();
			long firstSlideID = Long.parseLong(child.getString(2));

			long cid = Long.parseLong(child.getString(0));
			sb.append(builderHtmlById(cid));
			cid = Long.parseLong(child.getString(1));
			sb.append(builderHtmlById(cid));

			for (int y = 0; y < dotCount; y++) {
				sb.append(builderSlideHtmlById(firstSlideID, y, slideType));
			}

			if (dotCount == 0 && !viewedFromOutside) {
				sb.append(
						"<a class=\"pw-no-slide pw-widget pw-container pw-puppet pw-link swiper-slide\" style=\"background: #eee;text-align: center;color: #ccc; line-height: 10;\" href=\"#\">还没有幻灯片！</a>");
			}

		} else {
			for (Object i : child) {
				long cid = Long.parseLong((String) i);
				sb.append(builderHtmlById(cid));
			}
		}

		sb.append("<div class=\"pw-slideshow-dots\" style=\"display:" + (widget.getBoolean("show_dots") ? "block" : "none") + ";\">");
		if (dotCount > 0) {
			for (int i = 0; i < dotCount; i++) {
				// TODO: 只要类型为图片的？暂时用-2的方式处理一下，不用查表了
				// if(child.widget_def_name == 'container')
				sb.append("<span class=\"swiper-pagination-switch ");
				if (first) {
					first = false;
					sb.append("swiper-active-switch");
				}
				sb.append("\">");

				if (slideType == 2 && dynamicConfig != null) {
					String imagePath = getDataTable().getString(i, "Path");
					sb.append("<img src=\"" + imagePath + "\">");

				} else if (slideType == 2) {
					String _id = child.getString(i + 2);
					sb.append(getThumbnailHtml(Long.parseLong(_id)));
				}

				sb.append("</span>");
			}
		}
		sb.append("</div>");

		return sb.toString();
	}

	@SuppressWarnings("unused")
	public ArrayList<HashMap<String, String>> generateSpecialStylesRuleData(Style styleModel, boolean force) {
		ArrayList<HashMap<String, String>> ruleDataArray = new ArrayList<HashMap<String, String>>();
		if (!styleModel.hasSpecialRules()) {
			HashMap<String, String> stylesRuleData = new HashMap<String, String>();
			stylesRuleData.put("selector", styleModel.getSelector() + " .pw-slideshow-dots .swiper-pagination-switch");
			stylesRuleData.put("cssText", "background-color: " + styleModel.getSlideshowDotsColor());
			ruleDataArray.add(stylesRuleData);
		}
		return ruleDataArray;
	}

	private DataTable getDynamicData(int playerID) {
		ZCImagePlayer player = new ZCImagePlayer();
		player.setID(playerID);
		if (player.fill()) {
			if (ImageSourceCode.isAuto(player.getImageSource())) {
				AbstractExecuteContext sitePrefix = (new SiteIndexTemplate()).getContext(player.getSiteID(), null, false);
				DataTable dt = ImagePlayerBL.getAutoImageResource(player, sitePrefix);
				if (dt != null && dt.getRowCount() > 0) {
					String siteUrl = SiteUtil.getSiteURL(player.getSiteID(), null);
					if (!siteUrl.endsWith("/")) {
						siteUrl += "/";
					}
					for (DataRow dr : dt) {
						if (!dr.getString("Path").startsWith("http://")) {
							dr.set("Path", siteUrl + dr.getString("Path").replaceAll("/+", "/"));
						}
					}
				}
				return dt;
			} else {
				Q qb = new Q("select * from ZCImagePlayerResources");
				qb.append(" where ImagePlayerID=?", playerID);

				DataTable dt = qb.fetch();
				if (dt != null && dt.getRowCount() > 0) {
					com.zving.imageplayer.item.PlayerImageAPIMethod.dealLinkText(dt);
					// 处理图片路径
					com.zving.imageplayer.item.PlayerImageAPIMethod.dealImagePath(dt);
					// 处理图片跳转链接
					com.zving.imageplayer.item.PlayerImageAPIMethod.dealInternalURL(dt);
				}

				return dt;
			}
		} else {
			return null;
		}

	}

	private String builderSlideHtmlById(long id, int index, int type) {

		if (idClsMap == null) {
			buildIdClsMap();
		}

		String cls = idClsMap.get(id);
		if (StringUtil.isEmpty(cls)) {
			return "";
		}

		JSONObject clsJson = content.getJSONObject(cls);
		JSONObject widget = clsJson.getJSONObject(Long.toString(id));

		if (widget.getString("typeLabel").equals("container") && index >= 0) {

			DataTable dt = getDataTable();
			StringBuilder sb = new StringBuilder();

			String url = dt.getString(index, "LinkURL");
			String text = dt.getString(index, "LinkText");
			String imagePath = dt.getString(index, "Path");

			sb.append("<a id=\"" + widget.getString("slug") + "\" class=\"pw-widget pw-container pw-puppet fid-" + widget.getString("id")
					+ " pw-link pw_" + widget.getString("slug") + " swiper-slide ");
			if (index > 1) {
				sb.append("swiper-slide-visible swiper-slide-active");
			}
			sb.append("\" data-id=\"" + widget.getString("id") + "\" ");
			sb.append("style=\"background-image:url(" + imagePath + ")\"");
			sb.append(" href=\"" + url + "\">");
			if (type == 1) {
				JSONArray child = widget.getJSONArray("child_containers");
				long textID = Long.parseLong(child.getString(0));

				String textCls = idClsMap.get(textID);
				JSONObject textClsJson = content.getJSONObject(textCls);
				JSONObject textWidget = textClsJson.getJSONObject(Long.toString(textID));

				sb.append("<div id=\"" + widget.getString("slug") + "\"");
				sb.append(" class=\"pw-widget pw-text pw-wf pw-puppet fid-" + textWidget.getString("id") + " pw_"
						+ textWidget.getString("slug") + "\"");
				sb.append(" data-id=\"" + textWidget.getString("id") + "\">");
				sb.append("<div class=\"pw-text\">" + text + "</div></div>");

			} else if (type == 2) {

			}
			sb.append("</a>");

			return sb.toString();
		} else {
			return builderHtmlById(id);
		}
	}

	private String getThumbnailHtml(long id) {
		StringBuilder sb = new StringBuilder();

		if (idClsMap == null) {
			buildIdClsMap();
		}

		String cls = idClsMap.get(id);
		if (StringUtil.isEmpty(cls)) {
			return "";
		}

		JSONObject clsJson = content.getJSONObject(cls);
		JSONObject widget = clsJson.getJSONObject(Long.toString(id));

		String thumbnail = widget.getString("thumbnail");

		if (thumbnail == null || thumbnail.isEmpty()) {
			return "";
		}

		sb.append("<img src=\"" + thumbnail + "\">");

		return sb.toString();
	}
}
