package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class Icons extends Base {
	public boolean hover;

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"b-container-icons\">");
		if (hover) {
			sb.append("<a class=\"icon-hand-click\" href=\"#\"></a>");
		}
		sb.append("</div>");
		return sb.toString();

	}
}
