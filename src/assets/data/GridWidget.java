package com.zving.pageweaver.widgets;

import java.util.ArrayList;
import java.util.HashMap;

import com.zving.framework.collection.CaseIgnoreMapx;
import com.zving.framework.collection.Mapx;
import com.zving.framework.data.DataTable;
import com.zving.framework.json.JSON;
import com.zving.framework.json.JSONArray;
import com.zving.framework.json.JSONObject;
import com.zving.framework.utility.StringUtil;
import com.zving.pageweaver.css.Style;
import com.zving.platform.IAPIDataFormat;
import com.zving.platform.IAPIMethod;
import com.zving.platform.api.APIRequest;
import com.zving.platform.api.APIResponse;
import com.zving.platform.api.format.JSONFormat;
import com.zving.platform.service.APIDataFormatService;
import com.zving.platform.service.APIMethodService;

public class GridWidget extends Base implements IContainerWidget {
	String getWidgetClass(JSONObject widget) {
		return "pw-grid";
	}

	public ArrayList<HashMap<String, String>> generateSpecialStylesRuleData(Style styleModel) {
		return generateSpecialStylesRuleData(styleModel, false);
	}

	public ArrayList<HashMap<String, String>> generateSpecialStylesRuleData(Style styleModel, boolean force) {
		ArrayList<HashMap<String, String>> ruleDataArray = new ArrayList<HashMap<String, String>>();
		if (force || !styleModel.hasSpecialRules()) {
			JSONObject gridSettings = styleModel.getWeaverGrid();
			if (gridSettings == null) {
				gridSettings = JSON.parseJSONObject("{\"columns\": [],\"is_reverse\": false}");
			}
			JSONArray columns = gridSettings.getJSONArray("columns");
			int columnCount = columns.length();
			boolean isReverse = gridSettings.getBoolean("is_reverse");
			for (int i = 0; i < columnCount; i++) {
				HashMap<String, String> stylesRuleData = new HashMap<String, String>();
				stylesRuleData.put("selector", generateSelector(styleModel.getSelector(), columnCount, i));
				stylesRuleData.put("cssText", generateCssText((JSONObject) columns.get(i), i == 0, isReverse));
				ruleDataArray.add(stylesRuleData);
			}
		}
		return ruleDataArray;
	}

	private String generateSelector(String classSelector, int columnCount, int columnIndex) {
		return classSelector + " > .pw-widget:nth-child(" + columnCount + "n + " + (columnIndex + 1) + ")" + ", " + classSelector
				+ " > .pw-cells .pw-cell:nth-child(" + columnCount + "n + " + (columnIndex + 1) + ")";
	}

	private String generateCssText(JSONObject columnStyle, boolean isFirst, boolean isReverse) {
		columnStyle.put("float", isReverse ? "right" : "left");
		columnStyle.put("clear", isFirst ? isReverse ? "right" : "left" : "none");
		columnStyle.put("max-width", "inherit");
		String cssString = "";
		for (String property : columnStyle.keySet()) {
			if (StringUtil.isEmpty(columnStyle.getString(property))) {
				continue;
			}
			cssString += property + ":" + columnStyle.getString(property) + ";";
		}
		return cssString;
	}

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		JSONArray child = widget.getJSONArray("child_containers");
		if (child != null && child.length() > 0) {
			JSONObject dynamicConfig = widget.getJSONObject("dynamic_config");
			if (dynamicConfig != null && dynamicConfig.getInt("count") > 0) {
				DataTable dt = new DataTable();
				String listType = dynamicConfig.getString("listType");
				if (StringUtil.isNotEmpty(listType) && listType.indexOf("ContentBlock") != -1) {// 存在listType配置
					dt = getDataByDynConfig(dynamicConfig);
					setDataTable(dt);
				}
				if (dt == null || dt.getRowCount() == 0) {
					sb.append("<div class=\"pw-text div-nodata\">暂无数据</div>");
				}
				for (Object i : child) {
					long cid = Long.parseLong((String) i);
					for (int j = 0, l = dynamicConfig.getInt("count"), dataCount = dt.getRowCount(); j < l && j < dataCount; j++) {
						this.setPuppetIndex(j);
						sb.append(builderHtmlById(cid));
					}
				}

			} else {
				for (Object i : child) {
					long cid = Long.parseLong((String) i);
					sb.append(builderHtmlById(cid));
				}
			}
		}
		return sb.toString();
	}

	private DataTable getDataByDynConfig(JSONObject dynamicConfig) {
		APIRequest request = new APIRequest();
		APIResponse response = new APIResponse();

		String methodID = dynamicConfig.getString("listType").toLowerCase();
		if (methodID.indexOf("contentblock") != -1) {
			// 自动列表才通过API去取
			IAPIDataFormat format = APIDataFormatService.getInstance().get(JSONFormat.ID);
			Mapx<String, Object> map = format.parse(JSON.toJSONString(dynamicConfig));
			map = new CaseIgnoreMapx<String, Object>(map);
			request.setParameters(map);
			IAPIMethod m = APIMethodService.getInstance().get(methodID);
			if (m == null) {
				response.setStatus(IAPIMethod.Status_MethodNotFound);
				response.setMessage("APIMethod " + methodID + " not found!");
			} else {
				response = m.invoke(request);
			}
		}
		DataTable dt = (DataTable) response.get("DataTable");
		int begin = dynamicConfig.getInt("begin");
		if (begin > 1) {
			for (int i = 0; i < begin - 1; i++) {
				dt.deleteRow(0);
			}
		}
		return dt;
	}
}
