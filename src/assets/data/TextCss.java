package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class TextCss extends Base {
	public String buildHtml(JSONObject style) {
		StringBuilder sb = new StringBuilder();
		if (style.getString("width") != null) {
			sb.append("min-width: 100%;");
		}
		if (style.getString("height") != null) {
			sb.append("height: " + style.getString("height") + ";");
		}
		if (style.getString("column_count") != null) {
			sb.append("column-count: " + style.getString("column_count") + ";");
			sb.append("-webkit-column-count: " + style.getString("column_count") + ";");
			sb.append("-moz-column-count: " + style.getString("column_count") + ";");
			if (style.getString("column_gap") != null) {
				sb.append("column-gap: " + style.getString("column_gap") + ";");
				sb.append("-webkit-column-gap: " + style.getString("column_gap") + ";");
				sb.append("-moz-column-gap: " + style.getString("column_gap") + ";");

			}
		}

		return sb.toString();
	}
}
