package com.zving.pageweaver.widgets;

import com.zving.contentcore.util.SiteUtil;
import com.zving.framework.json.JSONObject;

public class VideoWidget extends Base implements IContainerWidget {
	String getWidgetClass(JSONObject widget) {
		return "pw-video pw-wf";
	}

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		if (widget.getString("original_filename") != null) {
			String _width = widget.getString("original_width") != null ? widget.getString("original_width") : "100%";
			String _height = widget.getString("original_height") != null ? widget.getString("original_height") : "100%";
			String _thumbnail = widget.getString("thumbnail_url") != null ? widget.getString("thumbnail_url") : "";
			String _videoURL = widget.getString("original_filename");
			String _playerPath = isClientSide ? "/responsivepage/videojs/flowplayer.swf"
					: SiteUtil.getPreviewPrefix(siteID) + "responsivepage/videojs/flowplayer.swf";

			sb.append("<video");
			sb.append(" class=\"video-js vjs-default-skin vjs-big-play-centered\" controls=\"controls\" preload=\"auto\"");

			sb.append(" src=\"" + _videoURL + "\" data-setup=\"{}\"");
			sb.append(" width=\"" + _width + "\"");
			sb.append(" height=\"" + _height + "\"");
			sb.append(" poster=\"" + _thumbnail + "\"");
			sb.append(">");
			sb.append("<source src=\"" + _videoURL + "\" type='video/mp4' />");
			sb.append("<object id=\"flash_fallback_\" class=\"vjs-flash-fallback\" width=\"" + _width + "\" height=\"" + _height
					+ "\" type=\"application/x-shockwave-flash\" data=\"" + _playerPath + "\">");
			sb.append("<param name=\"movie\" value=\"" + _playerPath + "\" />");
			sb.append("<param name=\"allowfullscreen\" value=\"true\" />");
			sb.append("<param name=\"flashvars\" value='config={\"playlist\":[\"" + _thumbnail + "\", {\"url\": \"" + _videoURL
					+ "\",\"autoPlay\":false,\"autoBuffering\":true}]}' />");
			sb.append("<img src=\"" + _thumbnail + "\" width=\"" + _width + "\" height=\"" + _height + "\" alt=\"\"  title=\"\" />");
			sb.append("</object>");
			sb.append("</video>");

		}
		if (widget.getString("html") != null) {
			sb.append("<div class=\"pw-video-inner\">");
			sb.append(widget.getString("html"));
			sb.append("</div>");
		}

		return sb.toString();
	}
}
