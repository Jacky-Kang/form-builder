package com.zving.pageweaver.widgets;

import com.zving.framework.data.DataTable;

public interface IWidget {
	void setPuppetIndex(int pi);

	void setDataTable(DataTable dt);

	void setSiteID(long siteID);

	void setPageID(long pageID);
}
