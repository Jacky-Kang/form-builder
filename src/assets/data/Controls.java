package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class Controls extends Base {
	public boolean constrain;
	public String disabled_axis;

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"b-container-controls b-hidden\">");
		if (!constrain) {
			sb.append("<div class=\"b-container-controls-inner b-container-controls-resize b-container-controls-resize-n\"");
			sb.append("    data-direction=\"n\"></div>");
		}
		if (disabled_axis.equals("x")) {
			sb.append("<div class=\"b-container-controls-inner b-container-controls-resize b-container-controls-resize-ne\"");
			sb.append("     data-direction=\"ne\"></div>");
		}
		if (!constrain && !disabled_axis.equals("x")) {
			sb.append("<div class=\"b-container-controls-inner b-container-controls-resize b-container-controls-resize-e\"");
			sb.append("    data-direction=\"e\"></div>");
		}
		if (!disabled_axis.equals("x")) {
			sb.append("<div class=\"b-container-controls-inner b-container-controls-resize b-container-controls-resize-se\"");
			sb.append("    data-direction=\"se\"></div>");
		}
		if (!constrain) {
			sb.append("<div class=\"b-container-controls-inner b-container-controls-resize b-container-controls-resize-s\"");
			sb.append("    data-direction=\"s\"></div>");
		}
		if (!disabled_axis.equals("x")) {
			sb.append("<div class=\"b-container-controls-inner b-container-controls-resize b-container-controls-resize-sw\"");
			sb.append("    data-direction=\"sw\"></div>");
		}
		if (!constrain && !disabled_axis.equals("x")) {
			sb.append("<div class=\"b-container-controls-inner b-container-controls-resize b-container-controls-resize-w\"");
			sb.append("    data-direction=\"w\"></div>");
		}
		if (!disabled_axis.equals("x")) {
			sb.append("<div class=\"b-container-controls-inner b-container-controls-resize b-container-controls-resize-nw\"");
			sb.append("    data-direction=\"nw\"></div>");
		}
		sb.append("</div>");
		return sb.toString();

	}
}
