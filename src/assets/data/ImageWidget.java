package com.zving.pageweaver.widgets;

import com.zving.framework.Config;
import com.zving.framework.json.JSONObject;
import com.zving.framework.utility.StringUtil;

public class ImageWidget extends Base implements IContainerWidget {
	String getWidgetClass(JSONObject widget) {
		return "pw-img";
	}

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		if (widget.getString("thumbnail_url") != null) {
			String imgSrc = widget.getString("thumbnail_url");
			if (StringUtil.isEmpty(imgSrc)) {
				imgSrc = Config.getContextPath() + "pageweaver/images/nopicture.jpg";
			}
			sb.append("<img src=\"" + imgSrc + "\"");
			sb.append(widget.getString("alt_text") != null ? " alt=\"" + widget.getString("alt_text") + "\"" : "");
			sb.append(widget.getString("title_text") != null ? " title=\"" + widget.getString("title_text") + "\"" : "");
			sb.append(" />");

		}
		return sb.toString();
	}
}
