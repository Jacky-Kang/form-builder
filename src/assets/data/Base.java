package com.zving.pageweaver.widgets;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zving.contentcore.ContentCorePlugin;
import com.zving.contentcore.bl.SiteBL;
import com.zving.contentcore.item.PCPublishPlatform;
import com.zving.contentcore.util.InternalURLUtil;
import com.zving.contentcore.util.SiteUtil;
import com.zving.framework.Config;
import com.zving.framework.collection.Mapx;
import com.zving.framework.data.DataTable;
import com.zving.framework.json.JSONArray;
import com.zving.framework.json.JSONObject;
import com.zving.framework.template.TemplateWriter;
import com.zving.framework.utility.ObjectUtil;
import com.zving.framework.utility.StringUtil;
import com.zving.pageweaver.bl.ResponsivePageBL;
import com.zving.staticize.template.TemplateContext;
import com.zving.staticize.template.TemplateInstance;

public class Base implements IWidget {
	public boolean isClientSide = false;// 为了客户端生成html
	public boolean widgetIdsFromSlug = true; // 元素id取数据里的slug属性？
	public boolean supportsWidgetIdsFromSlug = true; // 元素id取数据里的slug属性？
	public boolean currentUserIsOwner = true; // 当前用户是否是本页面的编辑
	public boolean contentForBackend = true;
	public boolean viewedFromOutside = false;
	public long siteID;
	public long pageID;
	public long snapshotID;
	private int puppetIndex = 0;
	private DataTable dataTable;
	protected JSONObject content;
	protected Mapx<Long, String> idClsMap = null;

	public String builderHtmlById(long id) {
		if (content == null) {
			buildPageContent();
		}
		if (idClsMap == null) {
			buildIdClsMap();
		}
		String cls = idClsMap.get(id);
		if (StringUtil.isEmpty(cls)) {
			return "";
		}
		JSONObject clsJson = content.getJSONObject(cls);
		JSONObject widget = clsJson.getJSONObject(Long.toString(id));

		Class<?> clazz = null;
		try {
			clazz = Class.forName("com.zving.pageweaver.widgets." + cls);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		IWidget obj = null;
		try {
			obj = (IWidget) clazz.newInstance();
			if (puppetIndex > 0) {
				obj.setPuppetIndex(puppetIndex);
			}
			if (siteID > 0) {
				obj.setSiteID(siteID);
			}
			if (pageID > 0) {
				obj.setPageID(pageID);
			}
			if (dataTable != null) {
				obj.setDataTable(dataTable);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Method method = null;
		try {
			method = clazz.getMethod("buildHtml", JSONObject.class, boolean.class, JSONObject.class);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		String html = null;
		try {
			html = (String) method.invoke(obj, widget, contentForBackend, content);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		return html;
	}

	public boolean isContentForBackend() {
		return contentForBackend;
	}

	public void setContentForBackend(boolean contentForBackend) {
		this.contentForBackend = contentForBackend;
	}

	public String buildHtml(JSONObject widget, boolean contForBackend, JSONObject content) {
		this.content = content;
		contentForBackend = contForBackend;
		return buildHtml(widget);
	}

	public String buildHtml(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		String idFix = puppetIndex > 0 ? "_" + puppetIndex : "";
		String classFix = puppetIndex > 0 ? "pw-puppet " : "";
		viewedFromOutside = !isClientSide && !contentForBackend;
		JSONObject attrs = widget.getJSONObject("attributes");
		/*
		 * field_config数据形如
		 * {
		 * text: "<h4>${format(addtime,\"M月d日\")}</h4>",
		 * _expression: {
		 * format: "M月d日"
		 * }
		 * }
		 */
		JSONObject fieldConfig = widget.getJSONObject("field_config");
		// 配置了field_config的部件，要从父容器已经取到的dataTable中的数据进行动态解析得到要输出的html
		if (dataTable != null && fieldConfig != null) {
			for (String k : fieldConfig.keySet()) {
				if (!k.startsWith("_") && StringUtil.isNotEmpty(fieldConfig.getString(k))) {
					String expression = fieldConfig.getString(k);
					if (expression.indexOf("${") == -1) {
						expression = "${" + expression.trim() + "}";
					}
					int index = this.getPuppetIndex();
					if (dataTable.getRowCount() <= index) {
						widget.put(k, expression);
					} else {
						String v = expression;
						String field = expression;
						Pattern p = Pattern.compile("^\\$\\{(\\w+)\\}$", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
						Matcher matcher = p.matcher(expression);
						if (k.equals("href") && fieldConfig.getString(k).equals("url")) {
							v = InternalURLUtil.getInteralURL(dataTable.getLong(index, "CatalogID"), dataTable.getLong(index, "ID"));
						} else if (matcher.find()) {// 对于简单的占位直接从dataTable里取，不使用模板引擎
							field = matcher.group(1);
							v = dataTable.getString(index, field);
						} else {
							TemplateContext context = new TemplateContext(ContentCorePlugin.getStaticizeContext());
							Mapx<String, Object> data = dataTable.getDataRow(index).toMapx();
							for (Entry<String, Object> e : data.entrySet()) {
								context.addDataVariable(e.getKey(), e.getValue());
							}
							String templateContent = "<z:config type='Block' name='Block' />\n" + expression;
							String templatePath = SiteUtil.getSiteRoot(siteID, PCPublishPlatform.ID) + "template/";
							TemplateInstance tpl = ContentCorePlugin.getStaticizeContext().getTemplateManager().findBySource(templatePath,
									templateContent);
							tpl.setContext(context);
							TemplateWriter writer = new TemplateWriter();
							tpl.setWriter(writer);
							try {
								tpl.execute();
								v = writer.getResult();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						// 图片路径处理，唉，特殊处理逻辑写死在这不太优雅
						if (ObjectUtil.empty(v)) {
							v = expression;
							if (k.equals("original_filename") || k.equals("background_image")) {
								v = Config.getContextPath() + "/pageweaver/images/nopicture.jpg";
								widget.put("thumbnail_url", v);
							}
						}

						widget.put(k, v);
						// 对图片路径作单独处理？
						if (k.equals("original_filename") && v.length() > 0 && v.charAt(0) != '/' && v.indexOf("preview/") == -1) {
							widget.put("thumbnail_url", SiteUtil.getPreviewPrefix(siteID) + v);
						}
						if (k.equals("background_image") && v.length() > 0 && v.charAt(0) != '/' && v.indexOf("preview/") == -1) {
							widget.put("background_image", SiteUtil.getPreviewPrefix(siteID) + v);
						}
					}
				}
			}
		}

		if (attrs != null
				&& (attrs.getString("linktype") != null || (attrs.getString("href") != null && attrs.getString("href").equals("url")))) {
			sb.append("<a");
		} else {
			sb.append("<" + (widget.getString("tag") != null ? widget.getString("tag") : "div"));
		}

		JSONObject animationObj = widget.getJSONObject("animation");
		boolean hasAnimation = !isClientSide && !contentForBackend && ObjectUtil.notEmpty(animationObj);
		boolean hasBackgroundParallax = false;
		if (!isClientSide && widget.getJSONObject("background_parallax") != null
				&& widget.getJSONObject("background_parallax").getBoolean("enabled")
				&& widget.getJSONObject("background_parallax").getBoolean("speed")) {
			hasBackgroundParallax = true;
		}
		if (supportsWidgetIdsFromSlug) {
			sb.append(" id=\"" + widget.getString("slug") + idFix + "\"");
		} else {
			sb.append(" id=\"fr_" + widget.getString("id") + idFix + "\"");
		}

		sb.append(" class=\"pw-widget ");
		sb.append(classFix);
		sb.append(getWidgetClass(widget));
		sb.append(currentUserIsOwner ? " fid-" + widget.getString("id") : "");
		sb.append(widget.getString("html_class_string") != null ? " " + widget.getString("html_class_string") : "");
		sb.append(widget.getString("hover_class") != null ? " " + widget.getString("hover_class") : "");
		sb.append(attrs != null ? " pw-link" : "");
		sb.append(hasAnimation
				? " pw-having-animation pw-anim-result animated " + animationObj.getString("key")
						+ (animationObj.getBoolean("loop") ? " infinite" + (animationObj.getBoolean("alternate") ? " alternate" : "") : "")
				: "");
		if (hasBackgroundParallax) {
			sb.append(" pw-background-parallax ");
			if (!isClientSide && !contentForBackend) {
				sb.append(" pw-background-parallax-active");
			}
		}
		if (attrs != null && !attrs.isEmpty() && attrs.getString("linktype") != null && attrs.getString("linktype").equals("submit")) {
			sb.append(" pw-linktype-submit");
		}

		sb.append(getCssClasses(widget));

		sb.append("\"");
		if (widget.getString("background_image") != null) {
			sb.append("style=\"background-image:url(" + widget.getString("background_image") + ")\"");
		}
		sb.append(currentUserIsOwner ? " data-id=\"" + widget.getString("id") + "\"" : "");
		if (hasAnimation) {
			sb.append(" data-pw-animation-duration=\"" + animationObj.getString("duration") + "\"");
			sb.append(" data-pw-animation-delay=\"" + animationObj.getString("delay") + "\"");
		}
		sb.append(getAttributes(widget));
		if (attrs != null && !attrs.isEmpty()) {
			if (attrs.getString("email") != null) {
				sb.append(" href=\"mailto:" + attrs.getString("email"));
				if (attrs.getString("subject") != null) {
					sb.append("?subject=" + attrs.getString("subject"));
				}
				sb.append("\"");
			} else if (attrs.getString("url") != null) {
				sb.append(" href=\"" + attrs.getString("url") + "\"");
			} else if (attrs.getString("anchor") != null) {
				if (widgetIdsFromSlug) {
					sb.append(" href=\"#" + attrs.getString("anchor") + "\"");
				} else {
					sb.append(" href=\"#fr_" + attrs.getString("anchor") + "\"");
				}
			} else if (widget.getString("href") != null) {
				sb.append(" href=\"" + widget.getString("href") + "\"");
			}
			if (attrs.getString("target") != null) {
				sb.append(" target=\"" + attrs.getString("target") + "\"");
			}
			if (attrs.getString("linktype") != null && attrs.getString("linktype").equals("submit")) {
				sb.append(" tabindex=\"0\"");
			}
		}

		if (widget.getString("method") != null) {
			sb.append(" method=\"" + widget.getString("method") + "\"");
		}
		if (widget.getString("action") != null) {
			sb.append(" action=\"" + widget.getString("action") + "\"");
		}
		if (widget.getString("target") != null) {
			sb.append(" target=\"" + widget.getString("target") + "\"");
		}
		if (hasAnimation) {
			sb.append(" data-pw-animation='" + widget.getString("animation") + "'");
		}
		if (hasBackgroundParallax) {
			sb.append(" data-pw-background-parallax-speed=\"" + widget.getJSONObject("background_parallax").getString("speed") + "\"");
		}

		sb.append(">\n");

		sb.append(getContent(widget));

		if (viewedFromOutside) {
			sb.append(getWidgetScript(widget));
		}

		if (attrs != null
				&& (attrs.getString("linktype") != null || (attrs.getString("href") != null && attrs.getString("href").equals("url")))) {
			sb.append("\n</a>");
		} else {
			sb.append("\n</" + (widget.getString("tag") != null ? widget.getString("tag") : "div") + ">");
		}
		return sb.toString();
	}

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		JSONArray child = widget.getJSONArray("child_containers");
		if (child != null) {
			for (Object i : child) {
				long cid = Long.parseLong((String) i);
				sb.append(builderHtmlById(cid));
			}
		}
		return sb.toString();
	}

	String getWidgetScript(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		if (widget.getString("script") != null) {
			sb.append("<script>");
			sb.append("(function(){");
			sb.append("'use strict';");
			sb.append(widget.getString("script"));
			sb.append("}).call(document.getElementsByClassName('pw_");
			sb.append(widget.getString("slug"));
			sb.append("')[0]);");
			sb.append("</script>");
		}
		return sb.toString();
	}

	@SuppressWarnings("unused")
	String getAttributes(JSONObject widget) {
		return "";

	}

	@SuppressWarnings("unused")
	String getWidgetClass(JSONObject widget) {
		return "";
	}

	String getCssClasses(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		JSONArray css = widget.getJSONArray("css_classes");
		if (css != null && css.length() > 0) {
			for (Object i : css) {
				long cid = Long.parseLong((String) i);
				sb.append(" " + buildClassById(cid).toString());
			}
		}
		return sb.toString();
	}

	private StringBuilder buildClassById(long cid) {
		if (content == null) {
			buildPageContent();
		}
		JSONObject cssjson = content.getJSONObject("CssClass");
		StringBuilder sb = new StringBuilder();
		if (cssjson != null && cssjson.containsKey(Long.toString(cid))) {
			JSONObject obj = cssjson.getJSONObject(Long.toString(cid));
			if (obj.getString("_cls").equals("CssClass")) {
				sb.append(" " + obj.getString("name").substring(1));
			}
		}
		return sb;
	}

	/***
	 * 获取专题内容数据
	 */
	private void buildPageContent() {
		if (pageID == 0) {
			return;
		}
		if (snapshotID != 0) {
			content = ResponsivePageBL.getSnapshotContent(snapshotID);
		} else {
			content = ResponsivePageBL.getPageContent(pageID);
		}
	}

	/***
	 * 构建ID跟所属组件的关联关系
	 */
	protected void buildIdClsMap() {
		if (content != null) {
			idClsMap = new Mapx<Long, String>();
			for (String key : content.keySet()) {
				JSONObject json = content.getJSONObject(key);
				if (json != null) {
					for (String idKey : json.keySet()) {
						if (StringUtil.isDigit(idKey))
							idClsMap.put(Long.parseLong(idKey), key);
					}
				}
			}
		}
	}

	@Override
	public void setPuppetIndex(int prosthesesIndex) {
		puppetIndex = prosthesesIndex;
	}

	public int getPuppetIndex() {
		return puppetIndex;
	}

	public void setDataTable(DataTable dt) {
		dataTable = dt;
	}

	public DataTable getDataTable() {
		return dataTable;
	}

	public long getSiteID() {
		return siteID;
	}

	public void setSiteID(long id) {
		siteID = id;
	}

	public void setPageID(long pageid) {
		pageID = pageid;
	}

	public long getSnapshotID() {
		return snapshotID;
	}

	public void setSnapshotID(long snapshotID) {
		this.snapshotID = snapshotID;
	}

}
