package com.zving.pageweaver.widgets;

import com.zving.framework.json.JSONObject;

public class TextWidget extends Base implements IContainerWidget {
	String getWidgetClass(JSONObject widget) {
		return "pw-text pw-wf";
	}

	String getContent(JSONObject widget) {
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"pw-text\">");
		if (widget.getString("text") != null) {
			sb.append(widget.getString("text"));
		}
		sb.append("</div>");

		return sb.toString();
	}
}
