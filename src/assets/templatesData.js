import util from '../util/util'

const templatesData = [
  {
    type: '常用',
    title: '邮件订阅',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '邮件订阅',
        'help': '订阅成功后，我们将定期向您发送最新的产品和服务信息。'
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '您的Email地址',
        'placeholder': 'example@qq.com',
        'help': '建议用QQ邮箱或网易邮箱',
        'type': 'email',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }, {
          type: 'email',
          value: null,
          help: '请输入有效的Email地址'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  },

  {
    type: '常用',
    title: '意见反馈',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '意见反馈',
        'help': ''
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '反馈信息',
        'placeholder': '',
        'help': '最多140个字',
        'rows': '4',
        'min': '0',
        'max': '140',
        'validations': [],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': 'Email',
        'placeholder': '',
        'help': '我们将通过此Email与您联系，并承诺不会泄露给任何第三方。',
        'type': 'email',
        'validations': [{
          type: 'email',
          value: null,
          help: '请输入正确的Email地址'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  },

  {
    type: '常用',
    title: '用户满意度调查',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '用户满意度调查',
        'help': '为了更好地为您提供产品或服务，请您认真填写以下表单。'
      }
    }, {
      'component': 'SelectBasic',
      'setting': {
        'label': '您购买的是哪款产品？',
        'help': '',
        'list': '产品1\n产品2\n产品3\n产品4',
        'name': util.generateId()
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '购买原因',
        'placeholder': '您购买此产品的原因是什么？',
        'help': '最多140个字',
        'rows': '3',
        'min': '0',
        'max': '140',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'StarRating',
      'setting': {
        'label': '对产品的满意度',
        'help': '',
        'number': '5',
        'name': util.generateId()
      }
    }, {
      'component': 'StarRating',
      'setting': {
        'label': '对服务的满意度',
        'help': '',
        'number': '5',
        'name': util.generateId()
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '产品改进意见',
        'placeholder': '产品改进意见',
        'help': '最多140个字',
        'rows': '3',
        'min': '0',
        'max': '140',
        'validations': [
          {
            type: 'required',
            value: null,
            help: '内容不能为空'
          }
        ],
        'name': util.generateId()
      }
    }, {
      'component': 'RadioGroupInline',
      'setting': {
        'label': '您会再次购买我们的产品吗？',
        'help': '',
        'list': '会\n可能会\n不会',
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  },

  {
    type: '常用',
    title: '会员申请表格',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '会员申请表格',
        'help': '请如实填写，如有虚假取消参与资格。'
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '姓名',
        'placeholder': '',
        'help': '二到四个汉字',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }, {
          type: 'maxLength',
          value: 4,
          help: '最长不能错过4个字'
        }, {
          type: 'minLength',
          value: 2,
          help: '最短不能少于2个字'
        }, {
          type: 'zhCN',
          value: null,
          help: '请输入中文'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'AddressSelector',
      'setting': {
        'label': '地址',
        placeholder: '在这里输入详细地址',
        help: '',
        validations: [{
          type: 'required',
          value: null,
          help: '不能为空'
        }, {
          type: 'minLength',
          value: 2,
          help: '最小长度 2'
        }, {
          type: 'maxLength',
          value: 8,
          help: '最大长度 8'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'type': 'email',
      'setting': {
        'label': 'Email',
        'placeholder': 'example@qq.com',
        'help': '',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }, {
          type: 'email',
          value: null,
          help: '请输入正确的Email地址'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        label: '电话',
        placeholder: '',
        help: '',
        type: 'tel',
        validations: [{
          type: 'required',
          value: null,
          help: '不能为空'
        }, {
          type: 'minLength',
          value: 7,
          help: '最小长度 7'
        }, {
          type: 'maxLength',
          value: 11,
          help: '最大长度 11'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'RadioGroup',
      'setting': {
        'label': '会员级别',
        'help': '',
        'list': '普通-10元/月\n银卡-20元/月\n金卡-30元/月',
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  },

  {
    type: '常用',
    title: '会议报名表',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '会议报名表',
        'help': '2016年手机游戏大会，填写以下内容，抽取奖品。'
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '姓名',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        label: '电话',
        placeholder: '',
        help: '形式如19973998629',
        type: 'tel',
        validations: [{
          type: 'required',
          value: null,
          help: '不能为空'
        }, {
          type: 'minLength',
          value: 11,
          help: '请输入长度为11位的手机号码'
        }, {
          type: 'maxLength',
          value: 11,
          help: '请输入长度为11位的手机号码'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'RadioGroup',
      'setting': {
        'label': '参会日期',
        'help': '',
        'list': '技术专场（5月10日）\n工具专场（5月11日）\n运营专场（5月12日）',
        'name': util.generateId()
      }
    }, {
      'component': 'RadioGroupInline',
      'setting': {
        'label': '预计到会时间',
        'help': '',
        'list': '上午\n下午',
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  }, {
    type: '常用',
    title: '预约登记表',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '预约登记表',
        'help': '填写您的预约信息，将享受优先服务。'
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '姓名',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        label: '电话',
        placeholder: '',
        help: '形式如19973998629',
        type: 'tel',
        validations: [{
          type: 'required',
          value: null,
          help: '不能为空'
        }, {
          type: 'minLength',
          value: 11,
          help: '请输入长度为11位的手机号码'
        }, {
          type: 'maxLength',
          value: 11,
          help: '请输入长度为11位的手机号码'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'DateInput',
      'setting': {
        label: '预计到点时间:',
        help: '',
        min: '2018-01-01',
        max: '2018-12-30',
        validations: [],
        'name': util.generateId()
      }
    }, {
      'component': 'RadioGroupInline',
      'setting': {
        'label': '预计到店时间',
        'help': '',
        'list': '上午10点前\n上午10点后\n下午3点前\n下午3点后',
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  }, {
    type: '常用',
    title: '员工通讯录',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '员工通讯录',
        'help': '可以在内网站通讯录栏目进行查询。'
      }
    }, {
      'component': 'SelectBasic',
      'setting': {
        'label': '部门',
        'help': '',
        'list': '市场部\n人力资源部\n研发部\n测试部\n综合管理部',
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '姓名',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        label: '电话',
        placeholder: '',
        help: '形式如19973998629',
        type: 'tel',
        validations: [{
          type: 'required',
          value: null,
          help: '不能为空'
        }, {
          type: 'minLength',
          value: 11,
          help: '请输入长度为11位的手机号码'
        }, {
          type: 'maxLength',
          value: 11,
          help: '请输入长度为11位的手机号码'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '您的Email地址',
        'placeholder': 'example@qq.com',
        'help': '',
        'type': 'email',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }, {
          type: 'email',
          value: null,
          help: '请输入有效的Email地址'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'SelectBasic',
      'setting': {
        'label': '职位',
        'help': '',
        'list': '总经理\n副总经理\n营销总监\n产品经理\n客户经理\n工程师',
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  }, {
    type: '常用',
    title: '报销申请表',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '报销申请表',
        'help': '填报后先将发票交财务统计发票金额，然后经理审核通过后，方给予报销。'
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '报销人姓名',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'SelectBasic',
      'setting': {
        'label': '报销事项',
        'help': '',
        'list': '差旅费\n市场活动\n办公用品',
        'name': util.generateId()
      }
    }, {
      'component': 'NumberInput',
      'setting': {
        'label': '报销金额',
        'help': '',
        'min': 0,
        'max': 9999,
        'validations': [{
          type: 'required',
          value: null,
          help: '不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '报销金额详细说明',
        'placeholder': '',
        'help': '最多140个字',
        'rows': '3',
        'min': '0',
        'max': '140',
        'validations': [
          {
            type: 'required',
            value: null,
            help: '内容不能为空'
          }
        ],
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'buttonColor': 'btn-primary'
      }
    }]
  }, {
    type: '常用',
    title: '留言板',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '留言板',
        'help': '有事请留言，我们会尽快联系您。'
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '留言 ',
        'placeholder': '',
        'help': '最多140个字',
        'rows': '3',
        'min': '0',
        'max': '140',
        'validations': [
          {
            type: 'required',
            value: null,
            help: '内容不能为空'
          }
        ],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '姓名',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': 'Email',
        'placeholder': 'example@qq.com',
        'help': '',
        'type': 'email',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }, {
          type: 'email',
          value: null,
          help: '请输入有效的Email地址'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  },

  {
    type: '常用',
    title: '投诉平台',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '投诉平台',
        'help': '客户无小事，请详细说明您的不满。'
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '要投诉谁',
        'placeholder': '员工名、员工工号或部门名',
        'help': '',
        'type': 'text',
        'validations': [],
        'name': util.generateId()
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '投诉事由 ',
        'placeholder': '',
        'help': '最多140个字',
        'rows': '3',
        'min': '0',
        'max': '140',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        label: '联系电话',
        placeholder: '',
        help: '',
        type: 'tel',
        validations: [{
          type: 'required',
          value: null,
          help: '不能为空'
        }, {
          type: 'minLength',
          value: 7,
          help: '最小长度 7'
        }, {
          type: 'maxLength',
          value: 11,
          help: '最大长度 11'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'buttonColor': 'btn-primary'
      }
    }]
  }, {
    type: '分类测试',
    title: '社团纳新',
    data: [{

      'component': 'TitleLabel',
      'setting': {
        'label': '学院新媒体中心纳新表',
        'help': '欢迎您加入我们的团队！  请认真填写表单，我们会根据您填写的内容进行审核！',
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '姓名',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        label: '手机',
        placeholder: '',
        help: '',
        type: 'tel',
        validations: [{
          type: 'required',
          value: null,
          help: '不能为空'
        }, {
          type: 'minLength',
          value: 11,
          help: '请输入长度为11位的手机号码'
        }, {
          type: 'maxLength',
          value: 11,
          help: '请输入长度为11位的手机号码'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'CheckboxGroupInline',
      'setting': {
        'label': '性别',
        'help': '',
        'list': '男\n女\n其他',
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '学号',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'RadioGroupInline',
      'setting': {
        'label': '在读',
        'help': '',
        'list': '本科一年级\n本科二年级\n本科三年级\n本科四年级\n研究生',
        'name': util.generateId()
      }
    }, {
      'component': 'RadioGroupInline',
      'setting': {
        'label': '政治面貌',
        'help': '',
        'list': '中共党员（含预备）\n民主党派\n群众',
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '校内住址',
        'placeholder': '如：东篱居5栋',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '主要学生工作经历',
        'placeholder': '',
        'help': '140字以内',
        'rows': '3',
        'min': '0',
        'max': '140',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  }, {
    type: '常用',
    title: '客户拜访记录',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '客户拜访记录',
        'help': '',
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '客户名称',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'DateInput',
      'setting': {
        'label': '拜访日期',
        'help': '',
        'min': '2018-01-01',
        'max': '2018-12-30',
        'validations': [],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '被拜访人',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '被拜访人职位',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'SelectBasic',
      'setting': {
        'label': '联系渠道',
        'help': '',
        'list': '见面\n邮件\n电话\nQQ\n其他',
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '联系方式',
        'placeholder': '填电话、邮件地址等',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'SelectBasic',
      'setting': {
        'label': '成单可能性',
        'help': '',
        'list': '80%\n60%\n40%\n20%\n0%',
        'name': util.generateId()
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '备注',
        'placeholder': '',
        'help': '140字以内',
        'rows': '3',
        'min': '0',
        'max': '140',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  }, {
    type: '常用',
    title: '工作日报',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '工作日报',
        'help': '恭喜你又度过了充实的一天！'
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '汇报人',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'DateInput',
      'setting': {
        'label': '汇报日期',
        'help': '',
        'type': 'text',
        'validations': [],
        'name': util.generateId()
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '工作内容',
        'placeholder': '',
        'help': '140字以内',
        'rows': '3',
        'min': '0',
        'max': '140',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  }, {
    type: '分类测试',
    title: '绩效考核',
    data: [{
      'component': 'TitleLabel',
      'setting': {
        'label': '绩效考核',
        'help': '以下填写请实名填写，结果HR部门将严格保密。'
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '被考核人',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'SelectBasic',
      'setting': {
        'label': '所在部门',
        'help': '',
        'list': '市场部\n人力资源部\n研发部\n测试部\n综合管理部',
        'name': util.generateId()
      }
    }, {
      'component': 'NumberInput',
      'setting': {
        'label': '入职年限',
        'help': '加入本公司的年限，不足一年的算作0.5年',
        'min': '0',
        'max': '10',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'StarRating',
      'setting': {
        'label': '个人业务能力 ',
        'help': '',
        'number': '5',
        'name': util.generateId()
      }
    }, {
      'component': 'StarRating',
      'setting': {
        'label': '工作执行力和推动能力',
        'help': '',
        'number': '5',
        'name': util.generateId()
      }
    }, {
      'component': 'StarRating',
      'setting': {
        'label': '团队合作性',
        'help': '',
        'number': '5',
        'name': util.generateId()
      }
    }, {
      'component': 'StarRating',
      'setting': {
        'label': '沟通和协调能力',
        'help': '',
        'number': '5',
        'name': util.generateId()
      }
    }, {
      'component': 'StarRating',
      'setting': {
        'label': '工作主动性和责任心',
        'help': '',
        'number': '5',
        'name': util.generateId()
      }
    }, {
      'component': 'TextAreaInput',
      'setting': {
        'label': '评价',
        'placeholder': '请写出对该同事在工作态度、方式、承担压力等任何方面的评价',
        'help': '140字以内',
        'rows': '4',
        'min': '0',
        'max': '140',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'TextInput',
      'setting': {
        'label': '考核人（填表人）',
        'placeholder': '',
        'help': '',
        'type': 'text',
        'validations': [{
          type: 'required',
          value: null,
          help: '内容不能为空'
        }],
        'name': util.generateId()
      }
    }, {
      'component': 'ButtonAction',
      'setting': {
        'label': '',
        'button': '提交',
        'style': 'primary',
        'type': 'submit'
      }
    }]
  }
];

export default templatesData;
