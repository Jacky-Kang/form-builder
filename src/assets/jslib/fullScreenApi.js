// 全屏工具方法

var FullScreenApi = {
  browserPrefix: '',
  supportsFullScreen: false,
  isFullScreen: function () {
    switch (this.browserPrefix) {
      case '':
        return document.fullScreen
      case 'webkit':
        return document.webkitIsFullScreen
      default:
        return document[this.browserPrefix + 'FullScreen']
    }
  },
  fullscreenchange: function () {
    return (this.browserPrefix === '') ? 'fullscreenchange' : this.browserPrefix + 'fullscreenchange'
  },
  requestFullScreen: function (el) {
    return (this.browserPrefix === '') ? el.requestFullScreen() : el[this.browserPrefix + 'RequestFullScreen']()
  },
  cancelFullScreen: function (el) {
    return (this.browserPrefix === '') ? el.cancelFullScreen() : el[this.browserPrefix + 'CancelFullScreen']()
  },
  zoomInIcon: function (imgWrapEl) {
    switch (this.browserPrefix) {
      case '':
        imgWrapEl.style.cursor = 'pointer'
      case 'webkit':
        imgWrapEl.style.cursor = '-webkit-zoom-in'
      default:
        imgWrapEl.style.cursor = '-moz-zoom-in'
    }
  },
  zoomOutIcon: function (imgWrapEl) {
    switch (this.browserPrefix) {
      case '':
        imgWrapEl.style.cursor = 'pointer'
      case 'webkit':
        imgWrapEl.style.cursor = '-webkit-zoom-out'
      default:
        imgWrapEl.style.cursor = '-moz-zoom-out'
    }
  },
  init: function () {
    if (typeof document.cancelFullScreen !== 'undefined') {
      this.supportsFullScreen = true
    } else {
      var pfxArr = ['webkit', 'moz', 'ms', 'o', 'khtml']
      for (var i = 0, len = pfxArr.length; i < len; i++) {
        this.browserPrefix = pfxArr[i]
        if (typeof document[this.browserPrefix + 'CancelFullScreen'] !== 'undefined') {
          this.supportsFullScreen = true
          break
        }
      }
    }
  }
}

window.FullScreenApi = FullScreenApi
module.exports = FullScreenApi
