const z = window.Zving = window.Zving || {}

const getDom = function (el, doc) {
  if (!el) {
    return el
  }
  if (typeof el === 'string') {
    return (doc || document).getElementById(el.replace(/^#/, ''))
  }
  if (!el.nodeName && el.length && el[0] && el[0].nodeName) { // 如果是一个jquery对象
    return el[0]
  }
  return el
}
// 星级评定

const Rating = window.Rating = z.Rating = z.Rating ||  (function (elem, val) {
  var el = getDom(elem)
  if (!el) {
    return
  }
  var self = this
  this.hiddenInput = getDom('cmt_ContentScore')
  this.viewRating = getDom(el).getElementsByTagName('LI')[0]
  this.links = getDom(el).getElementsByTagName('A')
  this.starWidth = this.links[0].offsetWidth
  for (var i = 0, len = this.links.length; i < len; i++) {
    this.links[i].value = i + 1
    this.links[i].onclick = function () {
      self.setRate(this.value)
    }
    this.links[i].onmouseover = function () {
      for (var a = 0; a < this.value; a++) {
        self.links[a].className = self.links[a].className.replace(/ hcolor/g, '')
        self.links[a].className += ' hcolor'
      }
      for (var a = this.value; a < 5; a++) {
        self.links[a].className = self.links[a].className.replace(/ hcolor/g, '')
      }
    }
    this.links[i].onmouseout = function () {
      for (var a = 0; a < this.value; a++) {
        self.links[a].className = self.links[a].className.replace(/ hcolor/g, '')
      }
    }
  }
  if (val === undefined || val == null) {
    this.setRate(this.hiddenInput.value)
  } else {
    this.setRate(val)
  }
})
Rating.prototype = {
  setRate: function (val) {
    this.hiddenInput.value = val
    this.viewRating.style.width = val * this.starWidth + 0 + 'px'
    if (this.hiddenInput.value != 0) {
      for (var a = 0; a < this.hiddenInput.value; a++) {
        this.links[a].className = this.links[a].className.replace(/ select/g, '')
        this.links[a].className += ' select'
      }
      for (var a = this.hiddenInput.value; a < 5; a++) {
        this.links[a].className = this.links[a].className.replace(/ select/g, '')
      }
    }
  },
  getRate: function () {
    return this.hiddenInput.value
  }
}

module.exports = Rating
