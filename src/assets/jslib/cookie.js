/*! Cookie
 */

var CONTEXTPATH = window.CONTEXTPATH || '/'
const z = window.Zving = window.Zving || {}
const Cookie = window.Cookie = z.Cookie = z.Cookie || {}

Cookie.get = function (name) {
  var cs = document.cookie.split('; ')
  for (i = 0; i < cs.length; i++) {
    var arr = cs[i].split('=')
    var n = arr[0].trim()
    var v = arr[1] ? arr[1].trim() : ''
    if (n === name) {
      return decodeURIComponent(v)
    }
  }
  return null
}

Cookie.getAll = function () {
  var cs = document.cookie.split('; ')
  var r = {}
  for (i = 0; i < cs.length; i++) {
    var arr = cs[i].split('=')
    var n = arr[0].trim()
    var v = arr[1] ? arr[1].trim() : ''

    r[n] = decodeURIComponent(v)
  }
  return r
}

Cookie.set = function (name, value, expires, path, domain, secure, isPart) {
  if (name === undefined || value === undefined) {
    return false
  }
  if (!isPart) {
    value = encodeURIComponent(value)
  }
  if (!path) {
    path = CONTEXTPATH // 特别注意，此处是为了实现不管当前页面在哪个路径下，Cookie中同名名值对只有一份
  }
  path = path.replace(/^\w+:\/\/[.\w]+:?\d*/g, '') // 去掉CONTEXTPATH的主机名部分
  if (expires) {
    if (/^[0-9]+$/.test(expires)) {
      expires = new Date(new Date().getTime() + expires * 1000).toGMTString()
    } else {
      var date = util.date.parseDate(expires)
      if (date) {
        expires = date.toGMTString()
      } else {
        expires = undefined
      }
    }
  }
  if (!isPart) {
    Cookie.remove(name, path, domain)
  }
  var cv = name + '=' + value + ';' + ((expires) ? ' expires=' + expires + ';' : '') + ((path) ? 'path=' + path + ';' : '') + ((domain) ? 'domain=' + domain + ';' : '') + ((secure && secure !== 0) ? 'secure' : '')
  if (cv.length < 4096) {
    document.cookie = cv
  } else {
    throw '写入cookie中的数据过长' + cv
  }
  return true
}

Cookie.remove = function (name, path, domain) {
  var v = Cookie.get(name)
  if (!name || v === null) {
    return false
  }
  if (!path) {
    path = CONTEXTPATH // 特别注意，此处是为了实现不管当前页面在哪个路径下，Cookie中同名名值对只有一份
  }
  path = path.replace(/^\w+:\/\/[.\w]+:?\d*/g, '') // 去掉CONTEXTPATH的主机名部分

  document.cookie = name + '=;' + ((path) ? ' path=' + path + ';' : '') + ((domain) ? ' domain=' + domain + ';' : '') + ' expires=' + new Date(0) + ';'
  return true
}

window.Cookie = Cookie
module.exports = Cookie
