var z = window.Zving = window.Zving || {
  version: 3.0
}
var getCurrentScript = function (base) {
  if (document.currentScript) {
    return document.currentScript.src // FF,Chrome
  };
  var stack
  try {
    a.b.c() // 强制报错,以便捕获e.stack
  } catch (e) { // safari的错误对象只有line,sourceId,sourceURL
    stack = e.stack
    if (!stack && window.opera) {
      // opera 9没有e.stack,但有e.Backtrace,但不能直接取得,需要对e对象转字符串进行抽取
      stack = (String(e).match(/of linked script \S+/g) || []).join(' ')
    }
  }
  if (stack) {
    stack = stack.split(/[@ ]/g).pop() // 取得最后一行,最后一个空格或@之后的部分
    stack = stack[0] === '(' ? stack.slice(1, -1) : stack.replace(/\s/, '') // 去掉换行符
    return stack.replace(/(:\d+)?:\d+$/i, '') // 去掉行号与或许存在的出错字符起始位置
  }
  var nodes = (base ? document : document.head).getElementsByTagName('script') // 只在head标签中寻找
  for (var i = nodes.length, node; node = nodes[--i];) {
    if (node.readyState === 'interactive') {
      return node.src
    }
  }
  var node = nodes[nodes.length - 1]
  return node.hasAttribute ? node.src : node.getAttribute('src', 4)
}

var jspath = getCurrentScript(true)
var scripts = document.getElementsByTagName('script'),
  script = document.currentScript || scripts[scripts.length - 1]

z.JSLIBPATH = jspath.substr(0, jspath.lastIndexOf('/') + 1)
z.CONTEXTPATH = script.getAttribute('contextpath')

if (z.JSLIBPATH.indexOf(location.protocol + '//' + location.host + '/') == 0) {
  z.JSLIBPATH = z.JSLIBPATH.replace(location.protocol + '//' + location.host, '')
}
// 再外部没配置应用路径时才使用默认路径
if (!z.CONTEXTPATH) {
  z.CONTEXTPATH = z.JSLIBPATH.replace(/[^\/]+\/?$/, '')
  if (z.CONTEXTPATH.indexOf('/preview/') != -1) {
    z.CONTEXTPATH = z.CONTEXTPATH.substr(0, z.CONTEXTPATH.indexOf('preview/'))
  }
}

var toString = Object.prototype.toString
var ua = navigator.userAgent.toLowerCase()
z.isWindows = /windows|win32/.test(ua)
z.isMac = /macintosh|mac os|macintel|mac68k|macppc/.test(ua)
z.isLinux = /linux/.test(ua)
z.isIOS = /ipad|iphone|ipod/.test(ua)
z.isIPad = /ipad/.test(ua)
z.isAndroid = /android|slik\/\d/.test(ua)
z.isWPhone = /windows phone/.test(ua)
z.isDesktop = (z.isMac && !z.isIOS) || (z.isWindows && !z.isWPhone) || (z.isLinux && !z.isAndroid)
z.isTablet = /ipad|slik\/\d|(playbook).*?tablet/.test(ua) || (z.isAndroid && !/mobile/.test(ua))
z.isPhone = !z.isDesktop && !z.isTablet
z.inTouch = (('ontouchstart' in window) || ('createTouch' in document)) && !('onmousemove' in document.documentElement)
z.isGecko = /gecko/.test(ua) // 建议主要使用这个。包括Safari、firefox、chrome、IE11+、Opera15+。

z.ieVersion = /msie|trident/.test(ua) ? parseFloat(/(msie |rv:)([\w.]+)/.exec(ua)[2]) : null
z.isIE = /msie/.test(ua) && !!window.ActiveXObject
// 注意：IE11的navigator.userAgent不再有MSIE字样，也不推荐再使用window.ActiveXObject。
// IE版本不能仅依靠navigator.userAgent判断，因为高版本浏览器可以切换到多个低版本浏览器的渲染模式，所以要使用特性探测
// isIE6为判断IE6或更低版本，isIE11为判断IE11及更高版本，同时考虑在IE11下仿真低版本浏览器的情况。
z.isIE6 = z.isIE && !window.XMLHttpRequest // IE6及更低版本
if (z.isIE6) {
  z.ieVersion = 6
}
z.isIE7 = z.isIE && !!window.XMLHttpRequest && !window.XDomainRequest && !/msie 6/.test(ua)
if (z.isIE7) {
  z.ieVersion = 7
}
z.isIE8 = z.isIE && !!window.XDomainRequest && !/msie 7/.test(ua) // 在ie兼容模式下ua会返回msie 7
if (z.isIE8) {
  z.ieVersion = 8
}
z.isIE9 = z.isIE && !!window.performance && !/msie 8/.test(ua)// 注意在IE高版本仿真IE8时window.performance为true
if (z.isIE9) {
  z.ieVersion = 9
}
z.isIE10 = z.isIE && !!window.Worker && !!window.WebSocket && !/msie 9/.test(ua)
if (z.isIE10) {
  z.ieVersion = 10
}
z.isIE11 = z.isTrident && !!window.addEventListener && !!window.getSelection && !/msie 10/.test(ua) // 在IE高版本仿真IE10时window.getSelection、window.addEventListener为true
if (z.isIE11 && z.ieVersion < 11) {
  z.ieVersion = 11
}

z.isWindow = function (obj) {
  return obj.document && obj.navigator && !!obj.location
}
z.isEmpty = function (v, allowBlank) {
  return v === null || v === undefined || (toString.apply(v) === '[object Array]' && !v.length) || (!allowBlank ? v === '' : false)
}
/**
异步加载脚本
url:js文件路径，相对于引用js框架的页面，如果要从js框架根目录开始引用需自行加上z.JSLIBPATH
onsuccess:js文件加载后的回调函数
**/
z.loadJS = z.loadJs = function (url, onsuccess) {
  var head = document.getElementsByTagName('head')[0] || document.documentElement,
    script = document.createElement('script'),
    done = false
  script.src = url
  script.onerror = script.onload = script.onreadystatechange = function () {
    if (!done && (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')) {
      done = true
      if (onsuccess) {
        onsuccess()
      }
      script.onerror = script.onload = script.onreadystatechange = null
      // head.removeChild(script);
    }
  }
  head.appendChild(script)
}

/**
加载脚本
url:js文件路径，因有加z.PATH，所以路径是相对于js框架根目录开始
 **/
z.importJS = z.importJs = function (url) {
  if (!/^\/|^\w+\:\/\//.test(url)) {
    url = z.JSLIBPATH + url
  }
  if (!document.body || document.readyState == 'loading') {
    document.write('<script type="text/javascript" src="' + url + '"><\/script>')
  } else {
    z.loadJS(url)
  }
}

/**
异步加载CSS文件
url:css文件路径，相对于引用js框架的页面，如果要从js框架根目录开始引用需自行加上z.JSLIBPATH
 **/
// 往指定的同源页面窗口加载样式文件（求url为相对于win中页面的地址）
z.loadCSS = z.loadCss = function (url, win) {
  win = win && z.isWindow(win) ? win : window
  var document = win.document

  var head = document.getElementsByTagName('head')[0] || document.documentElement
  if (document.createStyleSheet) { // 注意：IE11的不再支持document.createStyleSheet
    document.createStyleSheet(url)
  } else {
    var e = document.createElement('link')
    e.rel = 'stylesheet'
    e.type = 'text/css'
    e.href = url
    head.appendChild(e)
  }
}
/**
加载CSS文件
url:css文件路径，因有加z.PATH，所以路径是相对于js框架根目录开始
 **/
z.importCSS = z.importCss = function (url, win) {
  win = win && z.isWindow(win) ? win : window
  var document = win.document

  if (!/^\/|^\w+\:\/\//.test(url)) {
    url = z.JSLIBPATH + url
  }
  if (!document.body || document.readyState == 'loading') {
    document.write('<link rel="stylesheet" type="text/css" href="' + url + '" />')
  } else {
    z.loadCSS(url)
  }
}
/**
 * 添加向页面内添加一个style标签，并添加规则
 * @param styleElId
 * @param cssStr
 */
z.addStyle = function (styleElId, cssStr, win) {
  // 当传人两个参数，且第二个参数为Window类型对象时，
  if (!(win && z.isWindow(win))) {
    if (cssStr && z.isWindow(cssStr)) {
      win = cssStr
      cssStr = null
    }
  }
  win = win && z.isWindow(win) ? win : window
  var document = win.document
  // 下面是以前的实现，只是在loadCSS中添加win参数

  if (!cssStr) {
    cssStr = styleElId
    styleElId = 'style' + z.pageId
  }

  /* 如果是一个不含{}的字符串，则认为是要载入外部的css */
  if (cssStr.indexOf('{') < 1 && cssStr.indexOf('}') < 1 && /^\S$/.test(cssStr)) {
    return z.loadCSS(cssStr, win)
  }
  var styleEl = document.getElementById(styleElId)
  if (!styleEl) {
    styleEl = document.createElement('style')
    styleEl.type = 'text/css'
    styleEl.id = styleElId
    document.getElementsByTagName('head')[0].appendChild(styleEl)
  }
  if (styleEl.styleSheet) { // 注意：IE11的不再支持style.styleSheet
    styleEl.styleSheet.cssText += cssStr
  } else {
    cssStr = document.createTextNode(cssStr)
    styleEl.appendChild(cssStr)
  }
  return styleEl
}
const mixIf = function (obj, srcObj, recursion) {
  if (!obj) {
    obj = {}
  }
  var p
  for (p in srcObj) {
    if (typeof (obj[p]) === 'undefined') {
      obj[p] = srcObj[p]
    } else if (recursion && typeof (obj[p]) === 'object' && obj[p] !== null && typeof (srcObj[p]) === 'object' && srcObj[p] !== null) {
      obj[p] = mixIf(obj[p], srcObj[p], recursion)
    }
  }
  return obj
}

// 当配置项namespace为window时，把Zving下的所有对象复制到window下
var p, skipExtra = []
var extraObject = /^(\$|Object|Class|Array|Date|Function|String|Node|Event|JSON|Storage|Comment)$/
for (p in z) {
  if (typeof (z[p]) === 'undefined' || /^(id|g|Helper)$/.test(p)) {
    continue
  }
  if (typeof (window[p]) !== 'undefined') {
    if (!extraObject.test(p)) {
      skipExtra.push('zRetouch.js -- ' + p + ' existed in window')
    } else if (extraObject.test(p) && typeof (z[p]) === 'object') { // 如果Zving下对象和原生对象重名，则复制对象下的属性/方法
      mixIf(window[p], z[p])
    }
  } else {
    try {
      window[p] = z[p]
    } catch (ex) {
      throw ex
    }
  }
}
if (skipExtra.length && console) {
  console.warn(skipExtra.join('\n'))
}

module.exports = z
