const z = window.Zving = window.Zving || {}
const Form = window.Form = z.Form = z.Form || {}

const getDom = function (el) {
  if (!el) {
    return el
  }
  if (typeof el === 'string') {
    return document.getElementById(el.replace(/^#/, ''))
  }
  if (!el.nodeName && el.length && el[0] && el[0].nodeName) { // 如果是一个jquery对象
    return el[0]
  }
  return el
}

Form.getForm = function (ele) {
  if (!ele) {
    return document.forms[0]
  } else {
    ele = getDom(ele)
    if (ele && ele.tagName.toLowerCase() !== 'form') {
      return null
    }
    return ele
  }
}

/*
			获得form的所有表单素，并把name及对应value存入JSON
			参数filter 为过滤函数,会被循环调用传递给item作参数要求返回布尔值判断是否过滤
			注意本方法由于使用键值对保存数据，相同name的值将被合并为用逗号（,）隔开的字符串
		*/

Form.getData = function (ele, filter) {
  var el = Form.getForm(ele)
  if (!el) {
    alert('查找表单元素失败!' + ele)
    return
  }
  filter = filter ||
    function (el) {
      return false
    }
  var dc = {}
  var els = el.elements,
    l = els.length,
    i
  var _push = function (name, value) {
      if (typeof dc[name] !== 'undefined') {
        if (!dc[name + '_JsonArray']) {
          dc[name + '_JsonArray'] = [dc[name]]
        }
        dc[name + '_JsonArray'].push(value)
        value = dc[name] + ',' + value + ''
      }
      dc[name] = value + ''
    }
  for (i = 0; i < l; i++) {
    el = els[i]
    var nameOrId = el.name || el.id
    if (!el.type || !nameOrId || filter(el)) {
      continue
    }
    switch (el.type) {
      case 'text':
      case 'textarea':
        if (el.getAttribute('placeholder') && el.value == el.getAttribute('placeholder')) {
          _push(nameOrId, '')
        } else {
          _push(nameOrId, el.value)
        }
        break
      case 'hidden':
      case 'password':
        _push(nameOrId, el.value)
        break
      case 'checkbox':
      case 'radio':
        if (el.name && el.checked) {
          _push(nameOrId, el.value)
        }
        break
      case 'select-one':
        if (el.selectedIndex > -1) {
          _push(nameOrId, el.value)
        }
        break
      case 'select-multiple':
        var opts = el.options
        for (var j = 0; j < opts.length; ++j) {
          if (opts[j].selected) {
            _push(nameOrId, opts[j].value)
          }
        }
        break
      case 'button':
        break
      default:
        if (el.name && el.id && el.value) { // 同时拥有type,name,id,value的元素也视为表单元素
          _push(nameOrId, el.value)
        }
        break
    }
  }
  return dc
}

module.exports = Form
