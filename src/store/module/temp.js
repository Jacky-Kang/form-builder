import templatesData from '../../assets/templatesData';

const state = {
  templatesData: templatesData
};

const getters = {
  getLength: state => state.templatesData.length,
  getTempByName: state => (name => {
    if (state.templatesData) {
      for (let i = 0, len = state.templatesData.length; i < len; i++) {
        if (state.templatesData[i].title === name) {
          return state.templatesData[i]
        }
      }
      return null;
    } else return null;
  }),
  getTemps: state => state.templatesData
};

const actions = {};

const mutations = {};

export default {
  name: 'temp',
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
