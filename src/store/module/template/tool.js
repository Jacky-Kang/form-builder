export default {
  isDisallowBlockTagsTask(state, id) {
    const node = state.template.find(item => item.id === id);
    if (node) {
      if (node.disallow_block_tags) return true;
      else if (node.parent_container) {
        return this.isDisallowBlockTagsTask(state, node.parent_container);
      } else {
        return false;
      }
    } else {
      return false;
    }
  },

  getRandom(len) {
    return Math.floor(Math.random() * Math.pow(10, len));
  },

  /**
   * 递归输出组件树,测试用
   * @param data
   * @param id
   * @param index
   * @param state
   */
  tree(data, id, index, state) {
    const node = data.find(item => item.id.toString() === id.toString());
    const str = Array(index + 1).join('｜－') + node._cls;
    console.log(str);
    state.tree += str + '<br/>';
    if (node.child_containers.length) {
      node.child_containers.forEach(cid => tree(data, cid, index + 1, state))
    }
  },

  getOptionTasker(state, id, tag) {
    const node = state.template.find(n => n.id === id);
    if (node) {
      if (node.form_tag === tag) return node;
      else if (node.child_containers && node.child_containers.length) {
        for (let i = 0, len = node.child_containers.length; i < len; i++) {
          const res = this.getOptionTasker(state, node.child_containers[i], tag);
          if (res) return res;
        }
      }
    }
    return undefined;
  }
};
