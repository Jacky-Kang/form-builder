import tool from './tool';
import Util from '../../../util/util';

export default {
  /**
   * 设置当前模板数据
   * @param state
   * @param data
   * @returns {*}
   */
  setCurrentData: (state, data) => state.template = data,

  /**
   * 设置方向
   * @param state
   * @param pos
   */
  setPos: (state, pos) => {
    state.pos = pos
  },

  /**
   * 设置当前编辑的节点id
   * @param state
   * @param id
   * @returns {*}
   */
  setEditId: (state, id) => state.editId = id,

  /**
   * 设置预览状态
   * @param state
   * @param status
   * @returns {*}
   */
  setPreview: (state, status) => state.preview = status,

  /**
   * 添加组件
   * @param state
   * @param addWidgetName
   * @param upId
   */
  addWidget: (state, {
    addWidgetName,
    upId
  }) => {
    const config = state.configs.find(item => item.name === addWidgetName);

    if (config && config.attr) {
      const attr = config.attr;
      const pos = state.pos;

      let disallowBlockTags = false;

      //目标节点
      const node = state.template.find(item => item.id === upId);
      //检查是否可以添加,根据disallow_block_tags属性向上递归判断
      if (node._cls === 'Section') {
        disallowBlockTags = tool.isDisallowBlockTagsTask(state, node.id);
      } else if (pos) {
        disallowBlockTags = tool.isDisallowBlockTagsTask(state, node.parent_container);
      } else {
        disallowBlockTags = tool.isDisallowBlockTagsTask(state, node.id);
      }

      if (!disallowBlockTags) {
        //使用默认值构建组件
        const widget = {};
        for (let key in attr) {
          let item = attr[key];
          if (item && typeof item === 'object' && item.type) {
            if (typeof item.default === "function") widget[key] = item.default();
            else widget[key] = item.default;
          } else {
            if (typeof item === "function") widget[key] = item();
            else widget[key] = item;
          }
        }

        //配置id
        widget.slug = widget.id = Util.getObjectId();
        if (widget.input_name) {
          widget.input_name = Util.generateId();
        }
        state.template.push(widget);

        //判断方向,如果没有方向则是向目标组件内部添加组件
        if (node._cls === 'Section') {
          widget.parent_container = node.id;
          switch (pos) {
            case 'top':
            case 'left':
              node.child_containers.splice(0, 0, widget.id);
              break;
            case 'right':
            case 'bottom':
            default:
              node.child_containers.push(widget.id);
              break;
          }
        } else if (pos) {
          //目标父节点
          const parentNode = state.template.find(item => item.id === node.parent_container);
          //目标节点所处位置
          const index = parentNode.child_containers.indexOf(upId);

          widget.parent_container = parentNode.id;

          //根据方向插入在父节点组件
          switch (pos) {
            case 'top':
            case 'left':
              parentNode.child_containers.splice(index, 0, widget.id);
              break;
            case 'right':
            case 'bottom':
              parentNode.child_containers.splice(index + 1, 0, widget.id);
              break;
          }
        } else {
          widget.parent_container = node.id;
          node.child_containers.push(widget.id);
        }
      }
    }
  },

  /**
   * 移动组件
   * @param state
   * @param opt
   */
  moveWidget: (state, {
    downId,
    upId
  }) => {
    const pos = state.pos;
    //需要移动的节点和父节点,以及节点所处位置
    const node = state.template.find(item => item.id === downId);
    const oldParentNode = state.template.find(item => item.id === node.parent_container);
    const oldIndex = oldParentNode.child_containers.indexOf(node.id);

    const targetNode = state.template.find(item => item.id === upId);

    //检查是否可以添加,根据disallow_block_tags属性向上递归判断
    let disallowBlockTags = false;
    if (pos) {
      disallowBlockTags = tool.isDisallowBlockTagsTask(state, targetNode.parent_container);
    } else {
      disallowBlockTags = tool.isDisallowBlockTagsTask(state, targetNode.id);
    }

    //防止父节点移动到自己下面
    let isOk = true;
    const check = function (oldId, newId) {
      if (oldId === newId) {
        isOk = false;
      } else if (isOk) {
        const node_c = state.template.find(item => item.id === oldId);
        node_c.child_containers.forEach(id => check(id, newId))
      }
    };

    check(downId, upId);


    if (!disallowBlockTags && isOk) {

      //从原来的父节点删除移动的节点
      oldParentNode.child_containers.splice(oldIndex, 1);

      //判断方向,如果没有方向则是向目标组件内部添加组件
      if (pos) {
        //目标节点和父节点,以及节点所处位置
        const newParentNode = state.template.find(item => item.id === targetNode.parent_container);
        const newIndex = newParentNode.child_containers.indexOf(targetNode.id);

        //改变节点父级
        node.parent_container = newParentNode.id;

        //插入到新的父节点
        switch (pos) {
          case 'top':
          case 'left':
            newParentNode.child_containers.splice(newIndex, 0, node.id);
            break;
          case 'right':
          case 'bottom':
            newParentNode.child_containers.splice(newIndex + 1, 0, node.id);
            break;
        }
      } else {
        node.parent_container = targetNode.id;
        targetNode.child_containers.push(node.id);
      }
    }
  },

  /**
   * 移除组件(递归)
   * @param state
   * @param removeId
   */
  removeWidget: (state, removeId) => {

    const task = function (id) {
      if (id) {
        //目标节点
        const node = state.template.find(item => item.id === id);

        if (node) {
          //如果有子组件,删除子组件
          let children = node.child_containers;
          if (children && children.length) {
            for (let i = 0, len = children.length; i < len; i++) {
              task(children[0])
            }
          }

          const index = state.template.indexOf(node);
          state.template.splice(index, 1);

          //从父容器删除自身
          const parentNode = state.template.find(item => item.id === node.parent_container);
          parentNode.child_containers.splice(parentNode.child_containers.indexOf(node.id), 1);
        }
      }
    };

    task(removeId);
  },

  /**
   * 添加片段
   * @param state
   * @param addFragmentName
   * @param upId
   */
  addFragment: (state, {
    addFragmentName,
    upId
  }) => {
    const config = state.fragments.find(item => item.tag === addFragmentName);
    if (config && config.data) {
      const data = JSON.parse(JSON.stringify(config.data));
      const pos = state.pos;
      const node = state.template.find(item => item.id === upId);

      //检查是否可以添加,根据disallow_block_tags属性向上递归判断
      let disallowBlockTags = false;
      if (node._cls === 'Section') {
        disallowBlockTags = tool.isDisallowBlockTagsTask(state, node.id);
      } else if (pos) {
        disallowBlockTags = tool.isDisallowBlockTagsTask(state, node.parent_container);
      } else {
        disallowBlockTags = tool.isDisallowBlockTagsTask(state, node.id);
      }

      if (node && !disallowBlockTags) {
        //构造数据,复写id(递归),防止id重复
        //构造字符串副本
        let dataStr = JSON.stringify(data);
        let mainId = '';
        //遍历原数据
        data.forEach(item => {
          let newId = Util.getObjectId();
          if (item.id === config.main) mainId = newId;
          dataStr = dataStr.replace(new RegExp(item.id, 'gm'), newId);
        });

        //追加数据
        const template = state.template.concat(JSON.parse(dataStr));
        const main = template.find(item => item.id === mainId);

        //处理main,连接数据
        if (node._cls === 'Section') {
          main.parent_container = node.id;
          //根据方向插入在父节点组件
          switch (pos) {
            case 'top':
            case 'left':
              node.child_containers.splice(0, 0, main.id);
              break;
            case 'right':
            case 'bottom':
            default:
              node.child_containers.push(main.id);
              break;
          }
        } else if (pos) {
          const parentNode = template.find(item => item.id === node.parent_container);
          main.parent_container = parentNode.id;

          //目标节点所处位置
          const index = parentNode.child_containers.indexOf(node.id);

          //根据方向插入在父节点组件
          switch (pos) {
            case 'top':
            case 'left':
              parentNode.child_containers.splice(index, 0, main.id);
              break;
            case 'right':
            case 'bottom':
              parentNode.child_containers.splice(index + 1, 0, main.id);
              break;
          }
        } else {
          main.parent_container = node.id;
          node.child_containers.push(main.id);
        }

        state.template = template;
      }
    }
  },

  /**
   * 改变当前模板
   * @param state
   * @param ID
   */
  setTemplateByID: (state, ID) => {
    state.template = undefined;
    const temp = state.templates.find(item => item.ID === ID);
    if (temp) state.template = temp.data;
  },

  setSelectedById: (state, id) => state.editId = id,

  /**
   * 在组件树种排序(就是在通一父组件内移动)
   * @param state
   * @param parentId
   * @param oldIndex
   * @param newIndex
   */
  sortWidgetByTree: (state, {
    parentId,
    oldIndex,
    newIndex
  }) => {
    const node = state.template.find(item => item.id === parentId);
    if (node) {
      const children = node.child_containers;
      const id = children.splice(oldIndex, 1)[0];
      children.splice(newIndex, 0, id);
    }
  },

  /**
   * 在组件树移动组件位置
   * @param state
   * @param parentId
   * @param newIndex
   * @param oldIndex
   * @param id
   */
  moveWidgetByTree: (state, {
    parentId,
    newIndex,
    oldIndex,
    id
  }) => {
    const node = state.template.find(item => item.id === id);
    console.log(node);
    if (node) {
      const oldParent = state.template.find(item => item.id === node.parent_container);
      oldParent.child_containers.splice(oldIndex, 1);
      node.parent_container = parentId;
      const newParent = state.template.find(item => item.id === parentId);
      newParent.child_containers.splice(newIndex, 0, node.id);
    }
    state.template = JSON.parse(JSON.stringify(state.template));
  },
};
