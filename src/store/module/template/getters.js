import tool from './tool';

export default {
  /**
   * 递归判断是否隐藏
   * @param state
   * @returns {function(*=): (boolean|*)}
   */
  isHided: state => id => {
    const isHidedTask = function (id) {
      const node = state.template.find(item => item.id === id);
      if (node) {
        const has = (' ' + node.html_class_string + ' ').indexOf(' hide ') > -1;
        if (has) return true;
        else if (node.parent_container) isHidedTask(node.parent_container);
        else return false;
      } else return false;
    };

    return isHidedTask(id);
  },

  getTemplates: state => state.templates,


  /**
   * 是否锁定
   * @param state
   * @returns {function(*=): (*|boolean|*)}
   */
  isDisallowBlockTags: state => id => {
    return tool.isDisallowBlockTagsTask(state, id);
  },

  /**
   * 获取组件图标(主要用于组件树)
   * @param state
   * @returns {function(*): (*|string)}
   */
  getWidgetIconByName: state => name => state.widgetIcons[name] || '',

  /**
   * 获取组件树
   * @param state
   * @returns {string|*}
   */
  getTree: state => state.tree,

  /**
   * 获取当前拖拽相对目标方向
   * @param state
   * @returns {string|*}
   */
  getPos: state => state.pos,

  /**
   * 获取当前模板数据
   * @param state
   * @returns {any}
   */
  getCurrentData: state => state.template,

  /**
   * 获取当前正在编辑的节点id
   * @param state
   * @returns {string|*}
   */
  getEditId: state => state.editId,

  /**
   * 组件配置信息
   * @param state
   * @returns {function(*): Array}
   */
  getConfigs: state => type => {
    let configs = [];
    switch (type) {
      case 'base':
        configs = state.configs;
        break;
      case 'fragment':
        configs = state.fragments;
        break;
    }

    return configs;
  },

  /**
   * 根据组件名称获取配置信息
   * @param state
   * @returns {function(*): *}
   */
  getConfigByName: state => name => state.configs.find(item => item.name === name),

  /**
   * 获取预览状态
   * @param state
   * @returns {boolean}
   */
  getPreviewStatus: state => state.preview,

  /**
   * 获取顶级容器
   * @param state
   * @returns {*}
   */
  getSection: state => {
    if (state.template) return state.template.find(item => item._cls === 'Section');
    else return null;
  },

  /**
   * 获取顶级容器ID
   * @param state
   * @param getters
   * @returns {*}
   */
  getSectionId: (state, getters) => {
    const section = getters.getSection;
    return section ? section.id : null;
  },

  /**
   * 根据id获得配置项
   * @param state
   * @returns {function(*): *}
   */
  getWidgetById: state => id => state.template.find((item) => item.id === id),

  /**
   * 获取子级id数组
   * @param state
   * @param getters
   * @returns {Function}
   */
  getChildrenIdById: (state, getters) => pid => {
    const item = getters.getWidgetById(pid);
    if (item) return item.child_containers;
    else return [];
  },

  /**
   * 获取子级配置项数组
   * @param state
   * @param getters
   * @returns {Function}
   */
  getChildrenById: (state, getters) => pid => {
    const ids = getters.getChildrenIdById(pid);
    return ids.map(id => state.template.find(temp => temp.id === id));
  },

  /**
   * 根据ID获取组件名称
   * @param state
   * @param getters
   * @returns {Function}
   */
  getClsById: (state, getters) => id => {
    const item = getters.getWidgetById(id);
    if (item) return item._cls;
    else return '';
  },

  /**
   * 递归获取父级lable
   * @param state
   * @returns {function(*=): Array}
   */
  getLabelsById: state => id => {
    const labels = [];

    if (id) {
      const task = function (node) {
        const parentNode = state.template.find(item => item.id === node.parent_container);
        if (parentNode && parentNode.parent_container) {
          labels.push({
            name: parentNode.label,
            id: parentNode.id
          });
          task(parentNode)
        }
      };

      const node = state.template.find(item => item.id === id);
      if (node && node.parent_container) task(node);
    }

    return labels;
  },

  /**
   * 通过form组件名称,获取对应配置信息
   */
  getFormConfigByName: state => name => {
    const fragment = state.fragments.find(f => f.tag === name);
    if (fragment) return fragment.config;
    else return undefined;
  },

  /**
   * 通过id和标签获取指定借点
   */
  getOptionByTag: state => (id, tag) => tool.getOptionTasker(state, id, tag),

  /**
   * 通过名称获取组件属性设置
   */
  getAttrByName: state => name => {
    const config = state.configs.find(c => c.attr._cls === name);
    if (config) return config.attr;
    else return undefined;
  },

};
