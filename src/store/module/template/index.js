import templates from "../../../assets/templates";
import {
  configs
} from '../../../components/widget/compolents';

import fragments from '../../../assets/fragment';

import getters from './getters';
import actions from './actions';
import mutations from './mutations';

const icons = {};
configs.forEach(item => {
  if (item.attr && item.attr._cls && item.icon) icons[item.attr._cls] = item.icon;
});

const state = {
  templates: templates, //数据集合(无数据)
  template: undefined, //当前模板
  preview: false, //是否为预览模式
  configs: configs, //组件参数配置集合
  widgetIcons: icons, //组件对应的图标
  fragments: fragments, //组件判断(表单组)
  editId: '', //当前编辑的节点ID
  pos: '', //拖拽时相对目标的方向(top,right,bottom,left)
  tree: '', //测试用组件树字符串,用于调试,
};

export default {
  name: 'template',
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
