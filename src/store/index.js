import Vue from 'vue'
import Vuex from 'vuex'

const modules = {};

(r => r.keys().forEach(key => {
  const module = r(key).default;
  modules[module.name] = module;
}))(require.context('./module', true, /^\.\/([A-z0-9-/]+\.js|[A-z0-9-/]+\/index\.js)$/));

Vue.use(Vuex);
export default new Vuex.Store({
  modules
});
