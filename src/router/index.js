import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home';
import TemaplateView from '../components/ui/TemaplateView';
import Editor from '../components/Editor'
import Designer from '../components/Designer'
import Home2 from '../components/Home2'
import Temp from '../components/widget/designer-components/ZTemp'


Vue.use(Router);

export default new Router({
  mode: 'history',
  hashbang: false,
  history: true,
  routes: [{
    base: '/',
    path: '/',
    name: '/',
    redirect: '/home2/temp',
  }, {
    path: '/home',
    name: 'name',
    meta: {title: '首页'},
    component: Home,
    children: [{
      path: 'temp/:name',
      name: 'temp-name',
      component: TemaplateView,
      meta: {title: '模板预览'},
      props: true
    }, {
      path: 'temp',
      name: 'temp',
      component: TemaplateView,
      meta: {title: '模板预览'}
    }]
  }, {
    path: '/editor/:name',
    name: 'editor',
    meta: {title: '模板编辑'},
    component: Editor,
    props: true
  }, {
    path: '/designer',
    name: 'designer',
    meta: {title: '页面设计'},
    component: Designer
  }, {
    path: '/designer/:ID',
    name: 'designer-ID',
    meta: {title: '页面设计'},
    props: true,
    component: Designer
  }, {
    path: '/home2',
    name: 'home2',
    meta: {title: '模板预览'},
    component: Home2,
    children: [{
      path: 'temp',
      name: 'temp2',
      meta: {title: '模板预览'},
      component: Temp
    }, {
      path: 'temp/:ID',
      name: 'temp2-ID',
      meta: {title: '模板预览'},
      props: true,
      component: Temp
    }
    ]
  }]
});
