const components = [];
const configs = [];

(r => r.keys().forEach(key => {
  const component = r(key).default;
  if (component) {
    components.push(component);
    if (component.config) {
      component.config.name = component.config.attr._cls;
      configs.push(component.config);
    }
  }
}))(require.context('./', true, /^\.\/[A-z0-9-/]+\.vue$/));

module.install = function (Vue) {
  Vue.mixin({
    created: function () {
      components.forEach(com => Vue.component(com.name, com));
    }
  });
};

export default module;

export {
  configs
}
