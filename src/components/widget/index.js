import Compolents from './compolents'
import Drag from './drag'

const module = {};

module.install = function (Vue) {
  Vue.use(Compolents);
  Vue.use(Drag);
};

export default module;
