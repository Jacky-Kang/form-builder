/**
 * 集中导出vuex的方法,使用时直接解构即可
 */

import {
  mapGetters,
  mapMutations
} from 'vuex'

const getters = mapGetters('template', {
  getTree_x: 'getTree',
  getPos_x: 'getPos',
  getCurrentData_x: 'getCurrentData',
  getEditId_x: 'getEditId',
  getConfigs_x: 'getConfigs',
  getConfigByName_x: 'getConfigByName',
  getPreviewStatus_x: 'getPreviewStatus',
  getSection_x: 'getSection',
  getSectionId_x: 'getSectionId',
  getWidgetById_x: 'getWidgetById',
  getChildrenIdById_x: 'getChildrenIdById',
  getChildrenById_x: 'getChildrenById',
  getClsById_x: 'getClsById',
  getTemplates_x: 'getTemplates',

  getLabelsById_x: 'getLabelsById',
  getWidgetIconByName_x: 'getWidgetIconByName',
  isHided_x: 'isHided',
  isDisallowBlockTags_x: 'isDisallowBlockTags',

  getFormConfigByName_x: 'getFormConfigByName',
  getAttrByName_x: 'getAttrByName',
  getOptionByTag_x: 'getOptionByTag',
});

const mutations = mapMutations('template', {
  setPos_x: 'setPos',

  setCurrentData_x: 'setCurrentData',

  setEditId_x: 'setEditId',
  setPreview_x: 'setPreview',

  addWidget_x: 'addWidget',
  moveWidget_x: 'moveWidget',
  removeWidget_x: 'removeWidget',
  addFragment_x: 'addFragment',

  setTemplateByID_x: 'setTemplateByID',

  setSelectedById_x: 'setSelectedById',

  sortWidgetByTree_x: 'sortWidgetByTree',
  moveWidgetByTree_x: 'moveWidgetByTree',
});

export {
  getters,
  mutations
}
