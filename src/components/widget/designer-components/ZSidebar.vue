<template>
  <div :class="getClass(position,border)" :style="getStyle(show,position,width,height)">
    <slot></slot>
  </div>
</template>

<script>
export default {
  name: 'ZSidebar',
  props: {
    width: {
      type: String,
      default: '280px',
    },
    height: {
      type: String,
      default: '200px',
    },
    position: {
      type: String,
      default: 'left',
    },
    show: {
      type: Boolean,
      default: true,
    },
    border: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      val: true,
    }
  },
  methods: {
    getClass(position, border) {
      return 'sidebar sidebar-' + position + (border ? ' border' : '')
    },
    getStyle(show, position, width, height) {
      switch (position) {
        case 'left':
          return {
            'background-color': '#fff',
            width: width,
            left: show ? 0 : '-' + width,
          }
        case 'right':
          return {
            'background-color': '#fff',
            width: width,
            right: show ? 0 : '-' + width,
          }
        case 'top':
          return {
            'background-color': '#fff',
            height: height,
            top: show ? 0 : '-' + height,
          }
        case 'bottom':
          return {
            'background-color': '#fff',
            height: height,
            top: show ? 0 : '-' + height,
          }
      }
    },
  },
}
</script>

<style scoped>
.sidebar {
  position: absolute;
  box-sizing: border-box;
  transition: left 500ms, right 500ms, top 500ms, bottom 500ms, width 500ms,
    height 500ms;
}

.sidebar-left,
.sidebar-right {
  top: 0;
  bottom: 0;
  overflow-y: auto;
  overflow-x: hidden;
}

.sidebar-left.border {
  border-right: 2px solid #0099ff;
}

.sidebar-right.border {
  border-left: 2px solid #0099ff;
}

.sidebar-top,
.sidebar-bottom {
  left: 0;
  right: 0;
  overflow-x: auto;
  overflow-y: hidden;
}

.sidebar-top.border {
  border-bottom: 2px solid #0099ff;
}

.sidebar-bottom.border {
  border-top: 2px solid #0099ff;
}
</style>
