import {
  Draggable
} from '@shopify/draggable'
import store from '../../store'

const drag = new Draggable(undefined, {
  draggable: '.drag',
  delay: 150
});


let target = null;

drag.removeContainer(document.body);

/**
 * 开始拖动时,修改拖拽产生的副本,调整宽高为原对象尺寸
 */
drag.on('drag:start', e => {
  const source = target = e.source;
  const mirror = e.mirror;
  mirror.style.height = source.offsetHeight + 'px';
  mirror.style.width = source.offsetWidth + 'px';
  mirror.style.zIndex = 2000 + '';
});

/**
 * 拖拽移动时,判断鼠标相当于目标对象的位置
 */
drag.on('drag:move', e => {
  const evt = e.sensorEvent.originalEvent;

  if (evt) {
    const height = evt.target.offsetHeight / 2;
    const width = evt.target.offsetWidth / 2;

    /*
    方向判断,优先左右
    水平1/4以内为左
    水平3/4以外为由
    垂直1/4以内为上
    垂直3/4以外为下
    否则为中间
     */
    let pos = '';
    if (evt.layerX * 2 < width) pos = 'left';
    else if ((evt.layerX - width) > width / 2) pos = 'right';
    else if (evt.layerY * 2 < height) pos = 'top';
    else if ((evt.layerY - height) > height / 2) pos = 'bottom';
    else pos = '';

    store.commit('template/setPos', pos);
  }
});

/**
 * 鼠标弹起进行处置操作
 */
window.addEventListener('mouseup', function () {
  store.commit('template/setPos', '');
  drag.downId = undefined;
  drag.upId = undefined;
  drag.addWidgetName = undefined;
  drag.addFragmentName = undefined;
});

/**
 * 拖拽结束,向状态服务传递参数,由状态服务处理增删改查逻辑
 */
drag.on('drag:stop', e => {
  if (drag.upId) {
    if (drag.addWidgetName) {
      store.commit('template/addWidget', {
        upId: drag.upId,
        addWidgetName: drag.addWidgetName
      });
    } else if (drag.downId) {
      store.commit('template/moveWidget', {
        upId: drag.upId,
        downId: drag.downId
      });
    } else if (drag.addFragmentName) {
      store.commit('template/addFragment', {
        upId: drag.upId,
        addFragmentName: drag.addFragmentName
      });
    }
  }

  // console.log('drag:stop', e)
});

drag.on('drag:over', e => {
  // console.log('drag:over', e);
});

drag.on('drag:out', e => {
  // console.log('drag:out', e)
});

module.install = function (Vue, options) {
  Vue.prototype.$drag = drag;
};

export default module;
